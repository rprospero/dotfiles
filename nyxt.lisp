(define-configuration (buffer web-buffer)
  ((default-modes (append '(vi-normal-mode) %slot-default%))))
(define-configuration (web-buffer)
  ((default-modes (append '(dark-mode) %slot-default%))))

;; switch-buffer-next switch-buffer-previous


;; (defvar *my-keymap* (make-keymap "my-map"))
;; (define-key *my-keymap*
;;   "[" 'nyxt/switch-buffer-next
;;   "]" 'nyxt/switch-buffer-previous)

;; (define-mode my-mode ()
;;   "Dummy mode for the custom key bindings in `*my-keymap*'."
;;   ((keymap-scheme (define-scheme
;;		   scheme:vi-normal *my-keymap*))))

;; (define-configuration (buffer web-buffer)
;;   ((default-modes (append '(my-mode) %slot-default))))

(define-configuration browser
    ((autofills
      (list
       (nyxt::make-autofill :key "Name" :fill "Adam Washington")
       (nyxt::make-autofill :key "Address" :fill "47 Washford Glen")
       (nyxt::make-autofill :key "Post Code" :fill "OX11 7PU")
       (nyxt::make-autofill :name "Hello Printer" :key "Function example" :fill
			    (lambda () (format nil "hello!")))))))

(start-swank)

(define-configuration prompt-buffer
  ((style
    (str:concat
     %slot-default%
     (cl-css:css
      '(
	(body
	 :background-color "#1c433c")
	(".source-content"
	 :background-color "#1c433c")
	("#prompt-area"
	 :background-color "#1c433c")
	(".source-content"
	 :background-color "#1c433c")
	(".source-content th"
	 :background-color "#4c775d")
	(table
	 :color "#91dbbe")))))))

(define-configuration internal-buffer
  ((style
    (str:concat
     %slot-default%
     (cl-css:css
      '((body
	 :background-color "#1c433c"
	 :color "#8DCDB3")
	(hr
	 :color "#4c775d")
	(.button
	 :color "#1c433c"
	 :background-color "#70b39c")))))))

(define-configuration window
  ((message-buffer-style
    (str:concat
     %slot-default%
     (cl-css:css
      '((body
	 :background-color "#1c433c"
	 :color "#91dbbe3")))))))

(define-configuration nyxt/style-mode::dark-mode
    ((nyxt/style-mode::style-associations
       (list (nyxt/style-mode::make-style-association :url "https://nyxt.atlas.engineer"
	 :style (cl-css:css
		'((body
		   :background-color "gray"))))))))
