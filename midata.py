import csv
import sys

def main(file_path):
    with open(file_path, "r", encoding="latin-1") as infile:
        reader = csv.reader(infile, delimiter=";")
        info = list(reader)
        info = info[1:-2]
    with open("latest.csv", "w") as outfile:
        outfile.write("date,,payee,amount,,\r\n")
        writer = csv.writer(outfile)
        writer.writerows(info)

if __name__ == "__main__":
    main(sys.argv[1])
