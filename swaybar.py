#!/usr/bin/env python3

import json
import datetime
import time
from sys import stdout


def vbar(v):
  if v < 0 : " "
  elif v * 8 < 1: return "▁"
  elif v * 8 < 2: return "▂"
  elif v * 8 < 3: return "▃"
  elif v * 8 < 4: return "▄"
  elif v * 8 < 5: return "▅"
  elif v * 8 < 6: return "▆"
  elif v * 8 < 7: return "▇"
  else: return "█"

def make_object(name, text, width=None):
    result = {}
    if type(text) is float:
        if text > 0.9:
            result["color"] = "#FFCC00"
        text = vbar(text)
    result["full_text"] = name + " " + str(text)
    result["short_text"] = text
    # result["urgent"] = True
    if width is not None:
        result["min_width"] = width
    return result

def loadavg():
    with open("/proc/loadavg", "r") as infile:
        fast = infile.readline().split()[0]
    return float(fast)

def mem():
    with open("/proc/meminfo", "r") as infile:
        total = infile.readline().split()[1]
        infile.readline()
        avail = infile.readline().split()[1]
    return 1 - float(avail) / float(total)

def net_gen():
    oldr = 0
    oldw = 0
    while True:
        with open("/proc/net/dev", "r") as infile:
            data = infile.readlines()[3].split()[1:]
            newr = int(data[0])
            neww = int(data[8])
            yield((newr - oldr, neww-oldw))
            oldr = newr
            oldw = neww


def main():
    i = 0
    net = net_gen()
    while True:
        elements = []
        (down, up) = next(net)
        elements.append(make_object("\uf093", up, width=130))
        elements.append(make_object("\uf019", down, width=130))
        elements.append(make_object("\uF2DB", mem()))
        elements.append(make_object("\uF108", loadavg() / 4))
        elements.append(make_object("\uF073", datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
        stdout.write(json.dumps(elements))
        stdout.write(",\n")
        stdout.flush()
        time.sleep(1)
        i += 1
    # print("Hello, world!")

if __name__ == "__main__":
    print('{"version":1}')
    print('[')
    main()
