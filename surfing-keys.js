// an example to create a new mapping `ctrl-y`
// api.mapkey('<ctrl-y>', 'Show me the money', function() {
    // Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
// });
api.mapkey('`', 'org-capture', function() {
  console.log("Key");
    var orgProtoString = 'org-protocol://capture?' + new URLSearchParams({template: 'wq', url:window.location.href, title: document.title, body:window.getSelection() });
  console.log(orgProtoString);
  window.open(orgProtoString, "_self");
});
api.mapkey('¬', 'org-store-link', function() {
  console.log("Key");
    var orgProtoString = 'org-protocol://store-link?' + new URLSearchParams({url:window.location.href, title: document.title});
  console.log(orgProtoString);
  window.open(orgProtoString, "_self");
});
api.mapkey(';E', 'Edit in emacs', function() {
  console.log("Key");
    var orgProtoString = 'org-protocol://open-source?' + new URLSearchParams({url:window.location.href});
  console.log(orgProtoString);
  window.open(orgProtoString, "_self");
});

api.mapkey(';o', '#7Send a link URL to the org-capture', function() {
    console.log("Org");
    console.log(api.Hints);
    api.Hints.create('*[href]', function(element) {
	var orgProtoString = 'org-protocol://capture?' + new URLSearchParams({template: 'wl', url:element.href, title: element.innerText});
	window.open(orgProtoString, "_self");
    });
});

// an example to replace `T` with `gt`, click `Default mappings` to see how `T` works.
// api.map('gt', 'T');

// an example to remove mapkey `Ctrl-i`
// api.unmap('<ctrl-i>');

// set theme
settings.theme = `
.sk_theme {
    background: var(--base01);
    color: var(--base05)
}
.sk_theme tbody {
    color: var(--base05);
}
.sk_theme input {
    color: var(--base06);
}
.sk_theme .url {
    color: var(--base03);
}
.sk_theme .annotation {
    color: var(--base0B);
}
.sk_theme .omnibar_highlight {
    color: var(--base07);
}
.sk_theme .omnibar_timestamp {
    color: var(--base0E);
}
.sk_theme .omnibar_visitcount {
    color: var(--base0C);
}
.sk_theme #sk_omnibarSearchResult ul li:nth-child(odd) {
    background: var(--base00);
}
.sk_theme #sk_omnibarSearchResult ul li.focused {
    background: var(--base02);
}
#sk_status, #sk_find {
    font-size: 20pt;

}`;

const hintsCss =
  " border: color:var(--base00); background-color: var(--base0B); background-image: none;";
api.Hints.style(hintsCss);
api.Hints.style(hintsCss, "text");
// click `Save` button to make above settings to take effect.</ctrl-i></ctrl-y>
