#!/bin/sh

cd /home/adam/Code/libar
nix-shell --run "cabal run" | /usr/bin/env lemonbar -g x26 -f "Monoid Nerd Font Mono-42" -f Symbola-42 -B "#1c433c"
