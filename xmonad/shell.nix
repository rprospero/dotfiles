{pkgs ? import <nixpkgs> {} }:

let
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};

in

pkgs.mkShell {
  buildInputs = [
    (pkgs.haskellPackages.ghcWithPackages (pkgs: [pkgs.Cabal pkgs.hlint pkgs.xmonad pkgs.xmonad-contrib pkgs.xmonad-extras]))
    # (all-hies.selection { selector = p: { inherit (p) ghc882; }; })
  ];
}
