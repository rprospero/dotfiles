{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}

import Control.Monad (filterM, liftM2, void)
import Control.Monad.Reader (ask, runReaderT)
import Control.Monad.Trans.Class (lift)
import Data.Map.Strict as M (fromList, lookup)
import Graphics.X11.ExtraTypes.XF86
import Local.Folder
import Local.Music
import Local.Prompt
import Local.Shrinker
import Local.Theme
import Local.Types
import Local.Vnc
import System.Posix.Env (putEnv)
import XMonad
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.Search
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeInactive
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.HintedGrid
import XMonad.Layout.LayoutModifier
import XMonad.Layout.Named
import XMonad.Layout.Spacing
import XMonad.Layout.Tabbed
import XMonad.Prompt
import XMonad.Prompt.Pass
import XMonad.Prompt.RunOrRaise
import XMonad.Prompt.Ssh
import XMonad.Prompt.Theme
import XMonad.Prompt.Window
import XMonad.Prompt.XMonad
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig (additionalKeys)
import XMonad.Util.Run

myWorkspaces :: [String]
myWorkspaces = ["main", "web", "emacs", "documents", "chat", "media", "7", "8", "9"]

workSpaceMangler :: String -> String
workSpaceMangler "web" = "\xf269"
workSpaceMangler "main" = "\xf313"
workSpaceMangler "emacs" = "\xe779"
workSpaceMangler "documents" = "\xf0f6"
workSpaceMangler "chat" = "\xf860"
workSpaceMangler "media" = "\xf036"
workSpaceMangler x = x

myManageHook =
  composeAll . concat $
    [ [className =? b --> doF W.focusDown | b <- myClassWebShifts],
      [className =? b --> viewShift "web" | b <- myClassWebShifts],
      [className =? b --> viewShift "emacs" | b <- myClassEmacsShifts],
      [className =? b --> viewShift "chat" | b <- myClassChatShifts],
      [className =? b --> viewShift "documents" | b <- myClassDocumentsShifts],
      [className =? b --> viewShift "media" | b <- myClassMediaShifts],
      [stringProperty "WM_NAME" =? ("Figure " ++ show b) --> doShift "documents" | b <- [1 .. 9] :: [Int]],
      [(role =? "gimp-toolbox" <||> role =? "gimp-dock") --> doFloat],
      [title =? "vn" --> viewShift "media"]

    ]
  where
    viewShift = doF . liftM2 (.) W.greedyView W.shift
    myClassWebShifts = ["Firefox", "Opera", "google-chrome", "google-chrome-stable", "Next", "Vivaldi-stable"]
    myClassEmacsShifts = ["Emacs"]
    myClassChatShifts = ["Pidgin", "Thunderbird", "Geary", "mutt", "Microsoft Teams - Preview"]
    myClassDocumentsShifts = ["Evince", "Zathura", "MantidPlot", "mantidworkbench", "Dissolve-GUI"]
    myClassMediaShifts = ["Gimp", "launcher-4"]
    role = stringProperty "WM_WINDOW_ROLE"

main :: IO ()
main = do
  putEnv "_JAVA_AWT_WM_NONREPARENTING=1"
  putEnv "SAL_USE_VCLPLUGIN=gen"
  withTheme $ do
    spawn "sh ~/setroot.sh"
    background <- _base00
    foreground <- _base05
    font <- _lemonFont
    cfg <- myConfig
    lift . xmonad . docks . withUrgencyHook NoUrgencyHook $ cfg

uniformBorder x = Border x x x x

myLayoutHook = do
  tcfg <- myTabConfig
  return $
    named "\xfd34" (tabbedBottom MyShrinker tcfg)
      ||| named "\xf0c8" Full
      ||| named "\xf07d" (spacingRaw True (uniformBorder 0) False (uniformBorder 10) True (Tall 1 (3 / 100) (1 / 2)))
      ||| named "\xf7c0" (spacingRaw True (uniformBorder 0) False (uniformBorder 10) True (Grid False))

myPP = do
  current <- _base0C
  visible <- _base05
  hidden <- _base03
  let highlight color x = "%{F" <> color <> "}" <> workSpaceMangler x <> "%{F-}"
  return $
    def
      { ppSep = "|",
        ppCurrent = highlight current,
        ppVisible = highlight visible,
        ppHidden = highlight hidden
      }

myConfig = do
  theme <- ask
  prompt <- defPrompt
  msp <- mySearchPrompt
  lyh <- myLayoutHook
  pp <- myPP
  folderP <- folderPrompt
  musicP <- musicPrompt
  vncP <- vncPrompt
  background <- _base00
  -- liftIO $ statusBar "/home/adam/Code/dotfiles/bar.sh" pp (\XConfig{modMask = modm} -> (modm, xK_b )) $ def
  return $ def
      { focusedBorderColor = background,
        normalBorderColor = background,
        handleEventHook = handleEventHook def <+> fullscreenEventHook <+> ewmhDesktopsEventHook,
        manageHook = manageDocks <+> myManageHook,
        layoutHook = avoidStruts lyh,
        -- logHook = logHook def <+> ewmhDesktopsLogHook <+> fadeInactiveCurrentWSLogHook 0.7 <+> (dynamicLogString pp >>= xmonadPropLog),
        logHook = dynamicLog,
        modMask = mod4Mask,
        terminal = "mlterm -e tmux new-session -A -s main",
        workspaces = myWorkspaces
      }
      `additionalKeys` [ ((0, xF86XK_AudioLowerVolume), spawn "amixer set Master 2%-"),
                         ((0, xF86XK_AudioMute), spawn "amixer set Master toggle"),
                         ((0, xF86XK_AudioRaiseVolume), spawn "amixer set Master 2%+"),
                         ((0, xF86XK_HomePage), spawn "vivaldi"),
                         ( (0, xF86XK_Search),
                           promptSearchBrowser
                             prompt
                             "/home/adam/.nix-profile/bin/vivaldi"
                             ( moreIntelligent $
                                 searchEngine "DuckDuckGo" "https://duckduckgo.com/?q="
                             )
                         ),
                         ((0, xF86XK_WWW), spawn "vivaldi"),
                         ((0, xF86XK_Explorer), spawn "vivaldi"),
                         ((0, xK_Print), spawn "scrot"),
                         ((controlMask, xK_Print), spawn "scrot -u"),
                         ((mod4Mask .|. mod1Mask, xK_e), spawn "emacsclient -c"),
                         ((mod4Mask .|. mod1Mask, xK_t), themePrompt msp),
                         ((mod4Mask .|. shiftMask, xK_n), withFocused $ windows . W.sink),
                         ((mod4Mask .|. shiftMask, xK_g), windowPromptBring msp),
                         ((mod4Mask .|. shiftMask, xK_m), addWorkspacePrompt prompt),
                         ((mod4Mask .|. shiftMask, xK_p), passTypePrompt msp),
                         ((mod4Mask .|. shiftMask, xK_s), vncP),
                         ((mod4Mask .|. shiftMask, xK_t), folderP),
                         ((mod4Mask .|. shiftMask, xK_v), withWorkspace msp (windows . W.shift)),
                         ((mod4Mask .|. shiftMask, xK_z), spawn "slock"),
                         ((mod4Mask, xK_BackSpace), removeEmptyWorkspace),
                         ((mod4Mask, xK_equal), spawn "amixer set Master 5%+"),
                         ((mod4Mask, xK_g), windowPrompt msp Goto allWindows),
                         ((mod4Mask, xK_m), musicP),
                         ((mod4Mask, xK_minus), spawn "amixer set Master 5%-"),
                         ((mod4Mask, xK_p), runOrRaisePrompt prompt),
                         ((mod4Mask, xK_s), sshPrompt msp),
                         ((mod4Mask, xK_slash), spawn "amixer set Master toggle"),
                         ((mod4Mask, xK_t), spawn "spacefm"),
                         ((mod4Mask, xK_v), selectWorkspace msp),
                         ((mod4Mask, xK_x), xmonadPrompt msp),
                         ( (mod4Mask, xK_i),
                           promptSearchBrowser
                             prompt
                             "/home/adam/.nix-profile/bin/vivaldi"
                             ( moreIntelligent $
                                 searchEngine "DuckDuckGo" "https://duckduckgo.com/?q="
                             )
                         )
                       ]

moreIntelligent :: SearchEngine -> SearchEngine
moreIntelligent (SearchEngine name site) = searchEngineF name f
  where
    f s =
      if or
        [ "http://" `isPrefixOf` s,
          "https://" `isPrefixOf` s,
          "ftp://" `isPrefixOf` s,
          ('.' `elem` s) && (' ' `notElem` s)
        ]
        then s
        else site s

myTabConfig :: (Monad m) => ConfigTransformer m Theme
myTabConfig = do
  activeColor <- _base08
  inactiveColor <- _base00
  urgentColor <- _base02
  activeTextColor <- _base00
  inactiveTextColor <- _base05
  urgentTextColor <- _base07
  activeBorderColor <- _base01
  inactiveBorderColor <- _base00
  urgentBorderColor <- _base02
  fontName <- _xftFont
  return $
    def
      { activeColor = activeColor,
        inactiveColor = inactiveColor,
        urgentColor = urgentColor,
        activeTextColor = activeTextColor,
        inactiveTextColor = inactiveTextColor,
        urgentTextColor = urgentTextColor,
        activeBorderColor = activeBorderColor,
        inactiveBorderColor = inactiveBorderColor,
        urgentBorderColor = urgentBorderColor,
        fontName = fontName
      }
