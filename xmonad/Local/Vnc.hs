{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Vnc (vncPrompt) where

import XMonad
import XMonad.Prompt
import Local.Prompt
import Local.Types

data VncOption = Ndx | Ndc | Ndl | Delft
  deriving (Show, Read, Eq, Ord, Enum, Bounded)

data VncControl = VncControl
instance XPrompt VncControl where
  showXPrompt VncControl = "Server: "

vncPrompt :: (Monad m) => ConfigTransformer m (X ())
vncPrompt = enumPrompt VncControl vncHandle

vncHandle :: XPConfig -> VncOption -> X ()
vncHandle p Ndx = spawn "vncviewer 130.246.37.143"
vncHandle p Ndc = spawn "vncviewer 130.246.37.222"
vncHandle p Ndl = spawn "vncviewer 130.246.38.116"
vncHandle p Delft = spawn "vncviewer larmor-delft-01"
vncHandle _ _ = spawn "vncviewer"
