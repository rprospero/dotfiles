module Local.Shrinker (MyShrinker(MyShrinker)) where

import           Control.Monad (msum)
import           Data.List (isSuffixOf, isInfixOf, isPrefixOf,nub,stripPrefix)
import           Data.Maybe (fromMaybe, fromJust)
import           XMonad.Layout.Tabbed

data NameSegment = Prefix String | Suffix String | Subst String String


replace :: String -> String -> String -> String
replace "" _ _ = ""
replace s old new =
  if old `isPrefixOf` s
  then new ++ replace (drop (length old) s) old new
  else head s:replace (tail s) old new

shrinkSegment :: String -> NameSegment -> Maybe String
shrinkSegment s (Prefix prefix) = stripPrefix prefix s
shrinkSegment s (Suffix suffix)
  | suffix `isSuffixOf` s = Just $ reverse $ drop (length suffix) $ reverse s
  | otherwise = Nothing
shrinkSegment s (Subst before after)
  | before `isInfixOf`  s = Just $ replace s before after
  | otherwise = Nothing

mySegments :: [NameSegment]
mySegments = [
  -- Subst "=>" "⇒",
  Suffix " - Mozilla Firefox",
  Suffix " - GIMP",
  Prefix "adam@dyn006107",
  Suffix " - Google Search",
  Suffix " - Hoogle"]

dropSegment :: String -> String
dropSegment s = fromMaybe (init s) . msum $ map (shrinkSegment s) mySegments

myShrinkText :: s -> String -> [String]
myShrinkText _ "" = [""]
myShrinkText s cs = cs:myShrinkText s (dropSegment cs)

data MyShrinker = MyShrinker
instance Show MyShrinker where show _ = ""
instance Read MyShrinker where readsPrec _ s = [(MyShrinker,s)]
instance Shrinker MyShrinker where
  shrinkIt = myShrinkText
