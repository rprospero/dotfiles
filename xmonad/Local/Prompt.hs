{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Prompt (defPrompt, enumPrompt, mySearchPrompt) where

import           Control.Monad.Reader (ask)
import           Data.Char (toLower, toUpper)
import           Data.List (isSuffixOf, isInfixOf, isPrefixOf,nub,stripPrefix)
import           XMonad
import           XMonad.Prompt
import           Local.Theme
import           Local.Types (ConfigTransformer)

defPrompt :: (Monad m) => ConfigTransformer m XPConfig
defPrompt = promptTheme def {historyFilter = nub}

promptTheme :: (Monad m) => XPConfig -> ConfigTransformer m XPConfig
promptTheme x = do
  fgColor <- _base05
  bgColor <- _base00
  fgHLight <- _base0C
  borderColor <- _base04
  font <- _xftFont
  return $ x {fgColor = fgColor,
              bgColor = bgColor,
              fgHLight = fgHLight,
              bgHLight = bgColor,
              font = font,
              borderColor = borderColor}

mySearchPredicate :: String -> String -> Bool
mySearchPredicate query item = all (`isInfixOf` item) . words $ query

mySearchPrompt :: (Monad m) => ConfigTransformer m XPConfig
mySearchPrompt = do
  p <- defPrompt
  return p{searchPredicate = mySearchPredicate
          , autoComplete = Just 1}

enumPrompt :: forall a. forall p. forall b. forall m. (XPrompt p, Bounded a, Read a, Show a, Enum a, Monad m) =>
  p -> (XPConfig -> a -> X ()) -> ConfigTransformer m (X ())
enumPrompt t action = do
  conf <- defPrompt
  return $ mkXPrompt t conf (enumComplete (minBound :: a)) (action conf . read . promptCaps)

enumComplete :: (Enum a, Show a) => a -> String -> IO [String]
enumComplete enum x = return . filter (isPrefixOf (map toLower x) . map toLower) . map show $ enumFrom enum

promptCaps :: String -> String
promptCaps [] = []
promptCaps (x:xs) = toUpper x:xs
