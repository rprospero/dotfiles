{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Types where

import Control.Monad.Reader
import Data.Yaml
import GHC.Generics
import XMonad

type ConfigTransformer = ReaderT ConfigType

data ConfigType = ConfigType {
  colors :: Base16
  , font :: String
  , fontSize :: Int
  }

data Base16 = Base16 {
  base00 :: String
  , base01 :: String
  , base02 :: String
  , base03 :: String
  , base04 :: String
  , base05 :: String
  , base06 :: String
  , base07 :: String
  , base08 :: String
  , base09 :: String
  , base0A :: String
  , base0B :: String
  , base0C :: String
  , base0D :: String
  , base0E :: String
  , base0F :: String
  } deriving (Generic)

instance FromJSON Base16
