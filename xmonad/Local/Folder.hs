{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Folder (folderPrompt) where

import Control.Monad (filterM)
import Data.List (isSuffixOf, isInfixOf, isPrefixOf,nub,stripPrefix)
import System.Directory (getDirectoryContents, doesDirectoryExist)
import System.FilePath ((</>),splitFileName)
import XMonad
import XMonad.Prompt
import Local.Prompt
import Local.Theme
import Local.Types

folderPrompt :: (Monad m) => ConfigTransformer m (X ())
folderPrompt = do
  p <- defPrompt
  return $ mkXPrompt Folder p directoryComplete (spawn . ("spacefm "++))

directoryComplete :: String -> IO [String]
directoryComplete x = do
  let (dir, cur) = splitFileName x
  dirs <- getDirectoryContents dir
  filterM doesDirectoryExist $ map (dir </>) $ filter (cur `isPrefixOf`) dirs

data Folder = Folder

instance XPrompt Folder where
  showXPrompt Folder = "Directory:  "
