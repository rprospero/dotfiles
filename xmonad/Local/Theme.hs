{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Theme (Base16
                    , _base00
                    , _base01
                    , _base02
                    , _base03
                    , _base04
                    , _base05
                    , _base06
                    , _base07
                    , _base08
                    , _base09
                    , _base0A
                    , _base0B
                    , _base0C
                    , _base0D
                    , _base0E
                    , _base0F
                    , _xftFont
                    , _lemonFont
                    , withTheme) where

import Control.Monad.Reader (runReaderT, ask)
import Data.Either (fromRight)
import Data.Yaml
import XMonad
import Local.Types


_base00 :: (Monad m) => ConfigTransformer m String
_base00 = ("#" <>) . base00 . colors <$> ask
_base01 :: (Monad m) => ConfigTransformer m String
_base01 = ("#" <>) . base01 . colors <$> ask
_base02 :: (Monad m) => ConfigTransformer m String
_base02 = ("#" <>) . base02 . colors <$> ask
_base03 :: (Monad m) => ConfigTransformer m String
_base03 = ("#" <>) . base03 . colors <$> ask
_base04 :: (Monad m) => ConfigTransformer m String
_base04 = ("#" <>) . base04 . colors <$> ask
_base05 :: (Monad m) => ConfigTransformer m String
_base05 = ("#" <>) . base05 . colors <$> ask
_base06 :: (Monad m) => ConfigTransformer m String
_base06 = ("#" <>) . base06 . colors <$> ask
_base07 :: (Monad m) => ConfigTransformer m String
_base07 = ("#" <>) . base07 . colors <$> ask
_base08 :: (Monad m) => ConfigTransformer m String
_base08 = ("#" <>) . base08 . colors <$> ask
_base09 :: (Monad m) => ConfigTransformer m String
_base09 = ("#" <>) . base09 . colors <$> ask
_base0A :: (Monad m) => ConfigTransformer m String
_base0A = ("#" <>) . base0A . colors <$> ask
_base0B :: (Monad m) => ConfigTransformer m String
_base0B = ("#" <>) . base0B . colors <$> ask
_base0C :: (Monad m) => ConfigTransformer m String
_base0C = ("#" <>) . base0C . colors <$> ask
_base0D :: (Monad m) => ConfigTransformer m String
_base0D = ("#" <>) . base0D . colors <$> ask
_base0E :: (Monad m) => ConfigTransformer m String
_base0E = ("#" <>) . base0E . colors <$> ask
_base0F :: (Monad m) => ConfigTransformer m String
_base0F = ("#" <>) . base0F . colors <$> ask

defaultBase16 :: Base16
defaultBase16 = Base16 {
  base00 = "F0F000"
  , base01 = "F00000"
  , base02 = "00F000"
  , base03 = "0000F0"
  , base04 = "000000"
  , base05 = "000000"
  , base06 = "F00000"
  , base07 = "F0F000"
  , base08 = "F0F0F0"
  , base09 = "00F0F0"
  , base0A = "000000"
  , base0B = "000000"
  , base0C = "000000"
  , base0D = "000000"
  , base0E = "000000"
  , base0F = "000000"
  }

withTheme :: ConfigTransformer IO () -> IO ()
withTheme action = do
  colors <- fromRight defaultBase16 <$> decodeFileEither "/home/adam/.config/theme.yaml"
  let theme = ConfigType {
    colors = colors
    , font = "Iosevka"
    , fontSize = 12
    }
  flip runReaderT theme action

_xftFont :: (Monad m) => ConfigTransformer m String
_xftFont = do
  theme <- ask
  return $ "xft:" <> font theme <> ":size=" <> show (fontSize theme)

_lemonFont :: (Monad m) => ConfigTransformer m String
_lemonFont = do
  theme <- ask
  return $ font theme <> "-" <> show (fontSize theme)
