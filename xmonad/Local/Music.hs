{-# LANGUAGE FlexibleContexts, DeriveGeneric, ScopedTypeVariables #-}
module Local.Music (musicPrompt) where

import           Control.Monad                (void)
import qualified Network.MPD as MPD
import           XMonad
import           XMonad.Prompt
import           XMonad.Prompt.MPD
import           Local.Prompt
import           Local.Types


data MPDControl = MPDControl

instance XPrompt MPDControl where
  showXPrompt MPDControl = "Command: "

data MusicOption = Add | Clear | Load | NextSong | Play | Pause | PreviousSong | Shuffle | Stop
  deriving (Show, Read, Eq, Ord, Enum, Bounded)

musicPrompt :: (Monad m) => ConfigTransformer m (X ())
musicPrompt = enumPrompt MPDControl musicHandle

musicHandle :: XPConfig -> MusicOption -> X()
musicHandle p Add = void $ addAndPlay MPD.withMPD p [MPD.Artist, MPD.Title]
musicHandle p Clear = void . liftIO $ MPD.withMPD MPD.clear
musicHandle p Load = void $ loadPlaylist MPD.withMPD p
musicHandle p NextSong = void . liftIO $ MPD.withMPD MPD.next
musicHandle p Pause = void . liftIO $ MPD.withMPD (MPD.pause True)
musicHandle p Play = void . liftIO $ MPD.withMPD (MPD.play Nothing)
musicHandle p PreviousSong = void . liftIO $ MPD.withMPD MPD.previous
musicHandle p Shuffle = void . liftIO $ MPD.withMPD (MPD.shuffle Nothing)
musicHandle p Stop = void . liftIO $ MPD.withMPD MPD.stop
