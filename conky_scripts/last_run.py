#!/usr/bin/env python

import json
import re
import requests
import subprocess
from lxml import etree

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def colorize(s, c):
    return "%{{F#{}}}{}%{{F-}}".format(c, s)

def transition(old, new, left=False):
    if left:
        return r"%{{F#{}}}%{{B#{}}}%{{B#{}}}%{{F#{}}}".format(old[1],new[1],new[1],new[0])
    return r"%{{F#{}}}%{{B#{}}}%{{F#{}}}".format(new[1],new[1],new[0])


r = requests.get(
    "http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1")

data = json.loads(r.content[7:-1])
with open("/home/adam/.config/theme.yaml", "r") as infile:
    colors = json.load(infile)

title = data["inst_pvs"]["TITLE"]["value"]
current = data["inst_pvs"]["BEAMCURRENT"]["value"]
state = data["inst_pvs"]["RUNSTATE"]["value"]
shutter = data["inst_pvs"]["SHUTTER"]["value"]
duration = data["inst_pvs"]["RUNDURATION"]["value"]

if shutter == "CLOSED":
    shutter_color = (colors["base00"], colors["base0A"])
else:
    shutter_color = (colors["base00"], colors["base09"])

if state == "SETUP":
    state_color = (colors["base00"], colors["base0B"])
elif state == "RUNNING":
    state_color = (colors["base00"], colors["base09"])
else:
    state_color = (colors["base00"], colors["base0A"])

if float(current.split()[0]) < 20:
    current_color = (colors["base0A"], colors["base00"])
else:
    current_color = (colors["base07"], colors["base00"])

old = (colors["base07"], colors["base00"])
title_color = (colors["base00"], colors["base09"])

base = transition(old, state_color)
base += state
base += transition(state_color, shutter_color)
base += shutter
base += transition(shutter_color, current_color)
base += current
base += transition(current_color, title_color)
base += title
base += transition(title_color, state_color, True)
base += duration
base += transition(state_color,  old, True)

print(base)

#print("{}|{}|{}|{}|{}".format(state, shutter, current, title, duration))
