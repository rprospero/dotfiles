require 'cairo'

function conky_main()
    if conky_window == nil then
	return
    end
    local cs = cairo_xlib_surface_create(conky_window.display,
					 conky_window.drawable,
					 conky_window.visual,
					 conky_window.width,
					 conky_window.height)
    local color = {0XCC99CC ,0X9999CC ,0xcc6666 ,0xffcc99 ,0x9999ff ,0xff9966 ,0xcc6699 ,0xffffff}
    local loose = {{name="TIME",value=conky_parse("${time %X}")},
       {name="DATE",value=conky_parse("${time %x}")},
       {name="HOST",value=conky_parse("$nodename")},
       {name="UPTIME",value=conky_parse("$uptime")},
       -- {name="SAMPLE",value=conky_parse("${execi 60 ~/Code/dotfiles/conky_scripts/last_run.py}")},
       {name="WIND",value=conky_parse("${weather http://tgftp.nws.noaa.gov/data/observations/metar/stations/ EGTK wind_speed} ${weather http://tgftp.nws.noaa.gov/data/observations/metar/stations/ EGTK wind_dir}")},
       {name="TEMPERATURE",value=conky_parse("${weather http://tgftp.nws.noaa.gov/data/observations/metar/stations/ EGTK temperature}°C")},
       {name="CLOUD",value=conky_parse("${weather http://tgftp.nws.noaa.gov/data/observations/metar/stations/ EGTK cloud_cover}")},
       {name="CONDITIONS",value=conky_parse("${weather http://tgftp.nws.noaa.gov/data/observations/metar/stations/ EGTK weather}")}}
    local beam = {
       {name="SAMPLE",value=conky_parse("${execi 60 curl -H 'Accept: application/json' 'http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1' | tail -c +8 | head -c -1 | jq -r '.inst_pvs.TITLE.value'}")},
       {name="STATE",value=conky_parse("${execi 60 curl -H 'Accept: application/json' 'http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1' | tail -c +8 | head -c -1 | jq -r '.inst_pvs.RUNSTATE.value'}")},
       {name="NEUTRON SHUTTER",value=conky_parse("${execi 60 curl -H 'Accept: application/json' 'http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1' | tail -c +8 | head -c -1 | jq -r '.inst_pvs.SHUTTER.value'}")},
       {name="PROTON CURRENT",value=conky_parse("${execi 60 curl -H 'Accept: application/json' 'http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1' | tail -c +8 | head -c -1 | jq -r '.inst_pvs.BEAMCURRENT.value'}")},
       {name="DURATION",value=conky_parse("${execi 60 curl -H 'Accept: application/json' 'http://dataweb.isis.rl.ac.uk:60000/?callback=jQuery&Instrument=larmor&_1' | tail -c +8 | head -c -1 | jq -r '.inst_pvs.RUNDURATION.value'}")},
    }
    local system = {
       {name="Emacs",value=conky_parse("${exec systemctl --user show emacs | grep ActiveState | tail -c +13}")},
       {name="Travis",value=conky_parse("${exec systemctl --user show travis | grep ActiveState | tail -c +13}")},
       {name="Syncthing",value=conky_parse("${exec systemctl --user show syncthing | grep ActiveState | tail -c +13}")},
       {name="Setxkbmap",value=conky_parse("${exec systemctl --user show setxkbmap | grep ActiveState | tail -c +13}")},
       {name="Reverse Tunnel",value=conky_parse("${exec systemctl --user show reverse_tunnel | grep ActiveState | tail -c +13}")},
       {name="gpg-agent",value=conky_parse("${exec systemctl --user show gpg-agent | grep ActiveState | tail -c +13}")},
       {name="kbfs",value=conky_parse("${exec systemctl --user show kbfs | grep ActiveState | tail -c +13}")},
       {name="keybase",value=conky_parse("${exec systemctl --user show keybase | grep ActiveState | tail -c +13}")},
       {name="mpd",value=conky_parse("${exec systemctl --user show mpd | grep ActiveState | tail -c +13}")},
       {name="pulseaudio",value=conky_parse("${exec systemctl --user show pulseaudio | grep ActiveState | tail -c +13}")}
    }
    local cr = cairo_create(cs)

    lcars_grid(cr, {x=0, y=320}, 1920, color, loose)
    if conky_parse("$nodename") == "ndw1748" then do
      lcars_grid(cr, {x=400, y=500}, 1520, color, beam)
    end

    cairo_destroy(cr)
    cairo_surface_destroy(cs)
    cr=nil
end

function lcars_grid(cr, pt, width, color, values)
    local extent = best_extent(cr, values)
    local count = 0
    for _ in pairs(color) do count = count + 1 end
    local cols = math.floor(width/(extent.lwidth+extent.pwidth+extent.lheight))
    for k,v in ipairs(values)
    do
       lcars_text(cr, extent, pt.x + ((k-1)%cols)*(extent.lwidth+extent.pwidth+10)+((k-1)%cols+1)*extent.lheight, pt.y + math.floor((k-1)/cols) * (20 + extent.lheight), color[k%count+1], v.name)
       plain_text(cr, pt.x + 10+((k-1)%cols)*(extent.lwidth+extent.pwidth+10)+extent.lwidth+((k-1)%cols+1)*extent.lheight, pt.y + math.floor((k-1)/cols) * (20 + extent.lheight), v.value)
    end
end

function color_convert(c)
return ( (c/0x10000) % 0x100)/255,( (c/0x100) % 0x100)/255,(c % 0x100)/255
end--local function

function plain_text(cr, x, y, value)
    local r,g,b = color_convert(0xFF9900)
    cairo_set_line_width(cr, 1)
    cairo_set_line_cap(cr, line_cap)
    cairo_set_source_rgba(cr,1,g,b,1)
    cairo_select_font_face(cr, "Swiss911 UCM BT")
    cairo_set_font_size(cr, 36)
    cairo_move_to(cr,x,y)
    cairo_show_text(cr, value)
    cairo_fill_preserve(cr)
    cairo_stroke(cr)
end

function lcars_text(cr, extents, x, y, color, value)
    local pad = 3
    local r,g,b = color_convert(color)
    cairo_set_line_width(cr, 1)
    cairo_set_line_cap(cr, line_cap)
    cairo_set_source_rgba(cr,r,g,b,1)
    cairo_select_font_face(cr, "Swiss911 UCM BT")
    cairo_set_font_size(cr, 36)
    cairo_rectangle(cr, x, y+pad, extents.lwidth+2*pad, -extents.lheight-2*pad)
    cairo_arc(cr, x, y-extents.lheight/2, extents.lheight/2+pad, math.pi/2, math.pi*1.5)
    cairo_fill_preserve(cr)
    cairo_stroke(cr)
    cairo_set_source_rgba(cr,0,0,0,1)
    cairo_move_to(cr,x,y)
    cairo_show_text(cr, value)
    cairo_stroke(cr)
end

function best_extent(cr, texts)
   local result = {lwidth=0, lheight=0, pwidth=0, pheight=0}
   local extents = cairo_text_extents_t:create()
   tolua.takeownership(extents)

   for key,value in ipairs(texts)
   do
      cairo_select_font_face(cr, "Swiss911 UCM BT")
      cairo_set_font_size(cr, 36)
      cairo_text_extents(cr, value.name, extents)
      if extents.height > result.lheight then result.lheight = extents.height end
      if extents.width > result.lwidth then result.lwidth = extents.width end
      cairo_select_font_face(cr, "Swiss911 UCM BT")
      cairo_set_font_size(cr, 36)
      cairo_text_extents(cr, value.value, extents)
      if extents.height > result.pheight then result.pheight = extents.height end
      if extents.width > result.pwidth then result.pwidth = extents.width end
   end
   return result
end
