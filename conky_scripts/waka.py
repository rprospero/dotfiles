#!/usr/bin/env python

import datetime
import subprocess
import requests


def format_project(project):
    """Turn the project into a printable string"""
    return "{} {}".format(project["name"], project["digital"])


def main():
    api_key = subprocess.check_output(["pass", "wakatime-api"]).decode("utf8")

    today = datetime.datetime.today()
    yesterday = today - datetime.timedelta(1)

    data = requests.get(
        "https://wakatime.com/api/v1/users/current/summaries?api_key="
        + api_key
        + "&start={}-{}-{}".format(yesterday.year,
                                   yesterday.month,
                                   yesterday.day)
        + "&end={}-{}-{}".format(today.year, today.month, today.day)).json()

    result = map(format_project, data["data"][-1]["projects"])

    print("  ".join(result))


main()
