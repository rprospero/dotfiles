#!/usr/bin/env python3

import sys
import os
import os.path
import subprocess

BASE = "/home/adam/.password-store"

def combine(base, name):
    if base == BASE:
        return name[:-4]
    return base[len(BASE)+1:] + "/" + name

def list_passwords():
    walker = [combine(base, name) for (base, _, names) in os.walk(BASE) for name in names if name.endswith(".gpg")]
    for x in walker:
        print(x)

def call_password(account):
    # os.system("pass -c "+account)
    subprocess.run(["nohup", "/home/adam/.nix-profile/bin/pass", "-c", account], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    # os.system("echo \"" + account + "\" >> /home/adam/pass.tmp")

def main():
    if len(sys.argv) == 1:
        list_passwords()
    else:
        call_password(sys.argv[1])

if __name__ == "__main__":
    main()
