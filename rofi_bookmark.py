#!/usr/bin/env python3

import sys
import os
import os.path
import subprocess
import sqlite3

BASE = "file:/home/adam/.mozilla/firefox/adam/places.sqlite?immutable=1"
URL = ""

specific = {"rycee": "", "dissolve": "⏣", "Dissolve": "⏣"}

general = {
    "github": "",
    "gitlab": "",
    "nixos": "",
    "reddit": "",
    "news.ycombinator": "",
    "lobste.rs": "🦞",
    "imdb": "",
    "stackoverflow": "",
    "stackexchange": "",
    "steam": "",
    "twitter": "",
    "youtube": "",
    "wikipedia": "",
    "dominos": "",
    "pizza": "",
    "python.org": "",
    "stfc.ac.uk": "🧪",
    "ukri.org": "🧪",
    "instagram": "",
    "facebook": "",
    "google": "",
    "duckduckgo": "",
    "element.": "",
    "mozilla.org": "",
    "cppreference.com": "",
}


def iconify(url):
    for k, v in specific.items():
        if k in url:
            return v
    for k, v in general.items():
        if k in url:
            return v
    return ""


def list_bookmarks():
    with sqlite3.connect(BASE, uri=True) as conn:
        books = conn.execute(
            "SELECT title, url FROM moz_places GROUP BY title ORDER BY visit_count DESC, last_visit_date DESC"
        )
        for book, url in books:
            if book:
                print("{}\t{}".format(iconify(url), book))


def call_bookmarks(mark):
    with sqlite3.connect(BASE, uri=True) as conn:
        if "\t" in mark:
            mark = mark.split("\t")[1]
        books = [
            b[0]
            for b in conn.execute(
                "SELECT url FROM moz_places WHERE title=? ORDER BY visit_count DESC, last_visit_date DESC",
                (mark,),
            )
        ]
        if books:
            url = books[0]
        else:
            url = mark

        if " " in url:
            command = ["/home/adam/.nix-profile/bin/firefox", "--search", url]
        else:
            command = ["/home/adam/.nix-profile/bin/firefox", url]

        subprocess.Popen(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def main():
    if len(sys.argv) == 1:
        list_bookmarks()
    else:
        call_bookmarks(sys.argv[1])


if __name__ == "__main__":
    main()
