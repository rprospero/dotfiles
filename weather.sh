#!/bin/sh

APPID=$(pass openweathermap)

curl "api.openweathermap.org/data/2.5/weather?id=2651269&units=metric&APPID=$APPID" | jq -r '.weather[0].icon, (.main.temp | round), (.wind.speed | round), .wind.deg' | awk -f ~/Code/dotfiles/weather.awk
