# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, ... }:

{

  boot.initrd.availableKernelModules = [ "xhci_pci" "ehci_pci" "ahci" "firewire_ohci" "usbhid" "usb_storage" "sd_mod" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/34c5aa2e-84c7-40a9-9c49-62adc2040fea";
      fsType = "ext4";
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/77B5-0395";
      fsType = "vfat";
    };
  fileSystems."/data" =
    { device = "/dev/disk/by-label/data";
    fsType = "xfs";
  };
  fileSystems."/seagate" =
    { device = "/dev/disk/by-label/Seagate";
    options = ["uid=li" "gid=users"];
    fsType = "exfat";
  };

  swapDevices = [ ];

  nix.maxJobs = lib.mkDefault 8;
  powerManagement.cpuFreqGovernor = "powersave";
}
