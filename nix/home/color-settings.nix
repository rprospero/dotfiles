{ config, pkgs, lib, ... }:

{
  options.colors = lib.mkOption {
    type = lib.types.attrsOf lib.types.str;
    default = { };
  };
  options.icon-path = lib.mkOption {
    type = lib.types.str;
    default = "";
  };
  options.font-name = lib.mkOption {
    type = lib.types.str;
    default = "Iosevka";
  };
  options.font-size = lib.mkOption {
    type = lib.types.int;
    default = 20;
  };
  config.xdg.configFile."theme.yaml".text =
    pkgs.lib.generators.toJSON { } config.colors;
}
