{ pkgs, ... }:

{
  programs.beets.enable = true;
  programs.beets.settings = {
    plugins = [ "chroma" ];
    import.copy = true;
    import.write = true;
    import.timid = false;
    directory = "/home/adam/Music";
  };
  services.mpd.enable = true;
  services.mpd.musicDirectory = "/home/adam/Music";
  # services.mpd.network.listenAddress = "any";
  home.packages = with pkgs; [ chromaprint mpc_cli mpg123 pavucontrol ];
  wayland.windowManager.sway.config = {
    keybindings = {
      "Mod4+m" = "mode mpd";
      "XF86AudioPrev" = "exec mpc prev";
      "XF86AudioNext" = "exec mpc next";
      "XF86AudioPlay" = "exec mpc toggle";
    };
    modes.mpd = {
      Escape = "mode default";
      Return = "mode default";
      m = "exec swaymsg mode default && mpc toggle";
      j = "exec mpc next";
      k = "exec mpc prev";
      c = "exec swaymsg mode default && mpc consume";
      r = "exec swaymsg mode default && mpc repeat";
      s = "exec swaymsg mode default && mpc random";
      a =
        "exec swaymsg mode default && mpc listall | grep -v .git | rofi -i -multi-select -dmenu | mpc add";
      i =
        "exec swaymsg mode default && mpc listall | grep -v .git | rofi -i -multi-select -dmenu | mpc insert";
      l =
        "exec swaymsg mode default && mpc lsplaylist | rofi -i -dmenu | mpc load";
      x =
        "exec swaymsg mode default && mpc playlist | nl | rofi -i -multi-select -dmenu | awk '{print $1}' | mpc del";
      "Shift+s" = ''
        exec swaymsg mode default && mpc save `rofi -dmenu -p "Playlist Name"`'';
      "Shift+r" = "exec swaymsg mode default && mpc single";
      "Shift+x" = "exec swaymsg mode default && mpc clear";
      "Mod4+m" = "exec swaymsg mode default && mpc toggle";
    };
  };
}
