{ pkgs, config, lib, nur, ... }:

{
  nixpkgs.overlays = [ nur.overlay ];
  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland;
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      adsum-notabs
      browserpass
      bypass-paywalls-clean
      consent-o-matic
      languagetool
      sponsorblock
      surfingkeys
      ublock-origin
    ];
    profiles.adam = {
      bookmarks = {
        dissolve.url = "https://github.com/disorderedmaterials/dissolve";
        azure.url = "https://dev.azure.com/DisorderedMaterials/Dissolve";
        home-manager.url = "https://rycee.gitlab.io/home-manager/options.html";
      };
      isDefault = true;
      settings = {
        "toolkit.legacyUserProfileCustomizations.stylesheets" = true;
        "privacy.resistFingerprinting" = true;
        "extensions.pocket.enabled" = false;
        "identity.fxaccounts.enabled" = false;
      };
      userContent = import ./colors.nix { inherit (config) colors; };
      userChrome = ''
        /* Hide tab bar in FF Quantum */
          #TabsToolbar {
            visibility: collapse !important;
            margin-bottom: 21px !important;
          }

          #sidebar-box[sidebarcommand="treestyletab_piro_sakura_ne_jp-sidebar-action"] #sidebar-header {
            visibility: collapse !important;
          }
      '';
    };
  };
  xdg.desktopEntries.org-protocol = {
    name = "org-protocol";
    comment = "Intercept calls from emacsclient to trigger custom actions";
    categories = [ "WebBrowser" ];
    # keywords="org-protocol";
    icon = "emacs";
    type = "Application";
    exec = "emacsclient -- %u";
    terminal = false;
    # startupWMClass="Emacs";
    mimeType = [ "x-scheme-handler/org-protocol" ];
  };
}
