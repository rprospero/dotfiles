{ config, pkgs, ... }:

let
  inherit (config) colors;
  python = let packageOverrides = self: super: { };
  in pkgs.python3.override {
    inherit packageOverrides;
    self = python;
  };
  mython = python.withPackages (pkgs:
    with pkgs; [
      autopep8
      bokeh
      coverage
      flake8
      h5py
      ipython
      jupyter
      lxml
      matplotlib
      mock
      numpy
      pandas
      pylint
      pytest
      # python-language-server
      scipy
      sphinx
      sympy
      woob
    ]);

in {
  home.packages = [ pkgs.mypy mython ];

  programs.matplotlib = {
    enable = true;
    config = {
      backend = "TkAgg";
      axes = {
        facecolor = colors.base00;
        edgecolor = colors.base05;
        grid = true;
        labelcolor = colors.base05;
        prop_cycle =
          "cycler('color',['${colors.base08}','${colors.base09}','${colors.base0A}','${colors.base0B}','${colors.base0C}','${colors.base0D}','${colors.base0E}','${colors.base0F}'])";
      };
      figure = {
        facecolor = colors.base00;
        edgecolor = colors.base00;
      };
      font.size = 24;
      text.color = colors.base05;
      xtick.color = colors.base05;
      ytick.color = colors.base05;
      grid.color = colors.base05;
      savefig.facecolor = colors.base00;
    };
  };
}
