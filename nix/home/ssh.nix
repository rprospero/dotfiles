{
  enable = true;
  matchBlocks = {
    "home" = {
      hostname = "rprosperohome.duckdns.org";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "Channing" = {
      hostname = "192.168.1.217";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "mini" = {
      hostname = "192.168.1.227";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "mactop" = {
      hostname = "192.168.1.247";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "limichaels";
    };
    "Sheffield" = {
      hostname = "slb88data.duckdns.org";
      user = "adam";
      forwardX11 = true;
    };
    "Isolde_bypass" = {
      proxyCommand = "ssh -q home nc 192.168.1.166 22";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "work_bypass" = {
      proxyCommand = "ssh -q home nc localhost 2222";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "work_from_home" = {
      hostname = "192.168.1.217";
      port = 2222;
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "asmodeus" = {
      hostname = "asmodeus.isis.cclrc.ac.uk";
      user = "adam";
    };
    "asmodeus_from_home" = {
      proxyCommand = "ssh -q work_from_home nc asmodeus.isis.cclrc.ac.uk 22";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "Isolde" = {
      hostname = "192.168.1.166";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "work" = {
      hostname = "rprosperowork.duckdns.org";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "adam";
    };
    "larmorlinux" = {
      proxyCommand = "ssh -q work_bypass nc larmorlnx01.isis.cclrc.ac.uk 22";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "auv61894";
    };
    "larmorlinux_bypass" = {
      hostname = "larmorlnx01.isis.cclrc.ac.uk";
      forwardX11 = true;
      forwardX11Trusted = true;
      user = "auv61894";
    };
    "scarf" = {
      hostname = "ui2.scarf.rl.ac.uk";
      forwardX11 = true;
      user = "auv61894";
    };
    "global" = {
      hostname = "rprosperoglobal.duckdns.org";
      user = "root";
    };
    "envs" = {
      hostname = "envs.net";
      user = "rprospero";
    };
  };
}
