{ config, pkgs, ... }:

let inherit (config) colors;

in {
  programs.rofi = {
    enable = true;
    extraConfig.modi = "window,run,ssh,pass:${../../rofi_pass.py},bookmarks:${
        ../../rofi_bookmark.py
      }";
  };
  xdg.configFile."rofi/config.rasi".text = ''
    /**
     * rofi -dump-theme output.
     * Rofi version: 1.7.0
     **/
    configuration {
        font: "${config.font-name} ${toString (config.font-size + 4)}";
        modi: "window,run,ssh,pass:${../../rofi_pass.py},bookmarks:${
          ../../rofi_bookmark.py
        }";
    }
    * {
        red:                         #${colors.base09};
        selected-active-foreground:  var(background);
        lightfg:                     #${colors.base06};
        separatorcolor:              var(foreground);
        urgent-foreground:           var(red);
        alternate-urgent-background: var(lightbg);
        lightbg:                     #${colors.base01};
        background-color:            #${colors.base00};
        border-color:                var(foreground);
        normal-background:           var(background);
        selected-urgent-background:  var(red);
        alternate-active-background: var(lightbg);
        spacing:                     2;
        blue:                        #${colors.base0C};
        alternate-normal-foreground: var(foreground);
        urgent-background:           var(background);
        selected-normal-foreground:  var(lightbg);
        active-foreground:           var(blue);
        background:                  #${colors.base00};
        selected-active-background:  var(blue);
        active-background:           var(background);
        selected-normal-background:  var(lightfg);
        alternate-normal-background: var(lightbg);
        foreground:                  #${colors.base05};
        selected-urgent-foreground:  var(background);
        normal-foreground:           var(foreground);
        alternate-urgent-foreground: var(red);
        alternate-active-foreground: var(blue);
    }
    element {
        padding: 1px ;
        cursor:  pointer;
        spacing: 5px ;
        border:  0;
    }
    element normal.normal {
        background-color: var(normal-background);
        text-color:       var(normal-foreground);
    }
    element normal.urgent {
        background-color: var(urgent-background);
        text-color:       var(urgent-foreground);
    }
    element normal.active {
        background-color: var(active-background);
        text-color:       var(active-foreground);
    }
    element selected.normal {
        background-color: var(selected-normal-background);
        text-color:       var(selected-normal-foreground);
    }
    element selected.urgent {
        background-color: var(selected-urgent-background);
        text-color:       var(selected-urgent-foreground);
    }
    element selected.active {
        background-color: var(selected-active-background);
        text-color:       var(selected-active-foreground);
    }
    element alternate.normal {
        background-color: var(alternate-normal-background);
        text-color:       var(alternate-normal-foreground);
    }
    element alternate.urgent {
        background-color: var(alternate-urgent-background);
        text-color:       var(alternate-urgent-foreground);
    }
    element alternate.active {
        background-color: var(alternate-active-background);
        text-color:       var(alternate-active-foreground);
    }
    element-text {
        cursor:           inherit;
        highlight:        inherit;
        text-color:       inherit;
    }
    element-icon {
        size:             1.0000em ;
        cursor:           inherit;
        text-color:       inherit;
    }
    window {
        padding:          5;
        background-color: var(background);
        border:           1;
    }
    mainbox {
        padding: 0;
        border:  0;
    }
    message {
        padding:      1px ;
        border-color: var(separatorcolor);
        border:       2px dash 0px 0px ;
    }
    textbox {
        text-color: var(foreground);
    }
    listview {
        padding:      2px 0px 0px ;
        scrollbar:    true;
        border-color: var(separatorcolor);
        spacing:      2px ;
        fixed-height: 0;
        border:       2px dash 0px 0px ;
    }
    scrollbar {
        width:        4px ;
        padding:      0;
        handle-width: 8px ;
        border:       0;
        handle-color: var(normal-foreground);
    }
    sidebar {
        border-color: var(separatorcolor);
        border:       2px dash 0px 0px ;
    }
    button {
        cursor:     pointer;
        spacing:    0;
        text-color: var(normal-foreground);
    }
    button selected {
        background-color: var(selected-normal-background);
        text-color:       var(selected-normal-foreground);
    }
    num-filtered-rows {
        expand:     false;
        text-color: #${colors.base03};
    }
    num-rows {
        expand:     false;
        text-color: #${colors.base03};
    }
    textbox-num-sep {
        expand:     false;
        str:        "/";
        text-color: #${colors.base03};
    }
    inputbar {
        padding:    1px ;
        spacing:    0px ;
        text-color: var(normal-foreground);
        children:   [ prompt,textbox-prompt-colon,entry,num-filtered-rows,textbox-num-sep,num-rows,case-indicator ];
    }
    case-indicator {
        spacing:    0;
        text-color: var(normal-foreground);
    }
    entry {
        text-color:        var(normal-foreground);
        cursor:            text;
        spacing:           0;
        placeholder-color: #${colors.base03};
        placeholder:       "Type to filter";
    }
    prompt {
        spacing:    0;
        text-color: var(normal-foreground);
    }
    textbox-prompt-colon {
        margin:     0px 0.3000em 0.0000em 0.0000em ;
        expand:     false;
        str:        ":";
        text-color: inherit;
    }
  '';
}
