{ pkgs, config, ... }:

let inherit (config) colors;

in {
  home.packages = with pkgs; [
    element-desktop-wayland
    skypeforlinux
    teams
    (zoom-us.overrideAttrs (old: {
      postFixup = old.postFixup + ''
        wrapProgram $out/bin/zoom-us --unset XDG_SESSION_TYPE
            '';
    }))
  ];

  wayland.windowManager.sway.config = {
    assigns.Chat = [
      { instance = "Pidgin"; }
      { app_id = "Element"; }
      { title = "Zoom Meeting"; }
      { title = "Zoom Cloud Meetings"; }
    ];
    assigns.Teams = [{ class = "Microsoft Teams"; }];
    floating.criteria = [{ title = "zoom"; }];
  };

  programs.pidgin = {
    enable = true;
    plugins = [
      pkgs.pidgin-osd
      pkgs.purple-slack
      pkgs.purple-facebook
      pkgs.purple-hangouts
      pkgs.purple-matrix
      pkgs.purple-discord
    ];
  };

  home.file.".purple/gtkrc-2.0".text = ''
    gtk-font-name = "${config.font-name} ${toString (config.font-size - 2)}"


    # The following section allows you to change the style of the conversation
    # window widgets, log viewer widget, and request and notify window widgets.
    style "imhtml-fix"
    {
    font_name = "${config.font-name} ${toString config.font-size}"
    }
    # Set the widget style for the conversation entry box
    widget "*pidgin_conv_entry" style "imhtml-fix"
    # Set the widget style for the conversation
    widget "*pidgin_conv_imhtml" style "imhtml-fix"

    # Set the widget style for the log viewer
    widget "*pidgin_log_imhtml" style "imhtml-fix"

    # Set the widget style for IMHtml input widgets in request dialogs
    widget "*pidgin_request_imhtml" style "imhtml-fix"
    # Set the widget style for IMHtml widgets in notify dialogs
    widget "*pidgin_notify_imhtml" style "imhtml-fix"

    style "purplerc_style"
    {
      base[NORMAL] = "#${colors.base00}"
      bg[NORMAL] = "#${colors.base03}"
      GtkTreeView::odd_row_color = ""
      GtkTreeView::even_row_color = ""
      text[NORMAL] = "#${colors.base05}"
      # Change the color of hyperlinks.
      GtkIMHtml::hyperlink-color = "#${colors.base0D}"
      # Change the color of the nick in highlighted messages, e.g. messages containing your nick
      GtkIMHtml::highlight-name-color = "#${colors.base0A}"
      # Change the color of the nick in received message
      GtkIMHtml::receive-name-color = "#${colors.base0B}"
      # Change the color of the nick in sent message
      GtkIMHtml::send-name-color = "#${colors.base0C}"
      # Change the color of the nick in action messages, e.g. "/me likes pidgin"
      GtkIMHtml::action-name-color = "#${colors.base09}"
      # Change the font of the typing notification in conversation history.
      GtkIMHtml::typing-notification-font = "italic light 8.0"
      # Change the color of the typing notification
      GtkIMHtml::typing-notification-color = "#${colors.base04}"
      # Disable the typing notification
      GtkIMHtml::typing-notification-enable = 0

      # The following settings will change the behaviour in all GTK+ applications
      # Change the cursor color
      GtkWidget::cursor-color    = "#${colors.base07}"
      GtkWidget::secondary-cursor-color = "#00FF00" #for mixed ltr and rtl
    }
    widget_class "*" style "purplerc_style"
  '';
}
