{ pkgs, lib, config, emacs, ... }:

let

  resource = pkg: src: (lib.overrideDerivation pkg (attrs: { inherit src; }));

in {
  home.sessionVariables.EDITOR = "emacsclient";
  programs.emacs.enable = true;
  programs.emacs.package = pkgs.emacsNativeComp;
  services.emacs.enable = true;
  xresources.properties."Emacs.font" =
    "${config.font-name}-${toString config.font-size}";

  nixpkgs.overlays = [ emacs.overlay ];

  programs.emacs.extraPackages = epkgs:
    with epkgs; [
      forge
      blimp
      ace-link
      ace-window
      alert
      all-the-icons
      all-the-icons-dired
      all-the-icons-completion
      avy
      base16-theme
      bbdb
      benchmark-init
      cask
      ccls
      cmake-mode
      conda
      dhall-mode
      diminish
      dired-collapse
      dired-imenu
      dired-quick-sort
      direnv
      elfeed
      elfeed-protocol
      elpher
      emamux
      emojify
      encourage-mode
      evil-collection
      evil-commentary
      evil-easymotion
      evil-escape
      evil-goggles
      evil-indent-plus
      evil-matchit
      evil-numbers
      evil-org
      evil-quickscope
      evil-surround
      evil-text-object-python
      expand-region
      fish-completion
      fish-mode
      flycheck
      flycheck-mypy
      forge
      general
      git-annex
      haskell-mode
      htmlize
      hydra
      hyperbole
      hyperspace
      jabber
      julia-mode
      just-mode
      keyfreq
      langtool
      ledger-mode
      link-hint
      lsp-haskell
      lsp-mode
      lsp-ui
      magit
      mingus
      modern-cpp-font-lock
      modus-themes
      nix-mode
      notmuch
      org
      org-drill
      # org-plus-contrib
      org-ref
      org-roam
      org-special-block-extras
      ox-reveal
      package-lint
      projectile
      purescript-mode
      py-autopep8
      qml-mode
      rainbow-delimiters
      realgud
      restclient
      renpy
      rust-mode
      s
      shx
      skeletor
      skewer-mode
      slack
      slime
      spaceline
      spaceline-all-the-icons
      sx
      systemd
      tldr
      todotxt
      treemacs
      twittering-mode
      typescript-mode
      use-package
      which-key
      whitespace-cleanup-mode
      window-purpose
      writegood-mode
      xterm-color
      yaml-mode
      yasnippet

      selectrum
      marginalia
      orderless
      consult
      embark
      corfu

      dash-docs
      dap-mode
      keychain-environment
    ];
}
