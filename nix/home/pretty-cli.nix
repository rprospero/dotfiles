{ config, pkgs, ... }:

let
  sep = "";
  starship-color = color: text:
    "[${sep} ${text}](bold black bg:#${color})[${sep}]($bold #${color})";

in {
  imports = [ ./bat.nix ];
  programs.direnv.enable = true;
  programs.direnv.enableFishIntegration = true;

  programs.fish = {
    enable = true;
    functions = {
      fish_user_key_bindings = ''
        bind \cs 'pet-select'
      '';
      pet-select = ''
        set -l query (commandline)
        pet search --query "$query" $argv | read cmd
        commandline $cmd
      '';
      prev = ''
        set line (echo $history[1])
        pet new $line
      '';

    };
    shellAliases = {
      ls = "exa";
      cat = "bat";
      sc = "systemctl";
      scst = "systemctl start";
      scsp = "systemctl stop";
      scrl = "systemctl reload";
      scrt = "systemctl restart";
      sce = "systemctl enable";
      scd = "systemctl disable";
      scs = "systemctl status";
      scsw = "systemctl show";
      sclu = "systemctl list-units";
      scluf = "systemctl list-unit-files";
      sclt = "systemctl list-timers";
      scc = "systemctl cat";
      scie = "systemctl is-enabled";

      scu = "systemctl --user";
      scust = "systemctl --user start";
      scusp = "systemctl --user stop";
      scurl = "systemctl --user reload";
      scurt = "systemctl --user restart";
      scue = "systemctl --user enable";
      scud = "systemctl --user disable";
      scus = "systemctl --user status";
      scusw = "systemctl --user show";
      sculu = "systemctl --user list-units";
      sculuf = "systemctl --user list-unit-files";
      scult = "systemctl --user list-timers";
      scuc = "systemctl --user cat";
      scuie = "systemctl --user is-enabled";
    };
  };

  programs.nushell = {
    enable = true;
    settings = {
      prompt = "__zoxide_hook; ~/.nix-profile/bin/starship prompt";
      startup = [
        "zoxide init nushell --hook prompt | save ~/.zoxide.nu"
        "source ~/.zoxide.nu"
      ];
    };
  };

  programs.starship = {
    enable = true;
    enableFishIntegration = true;
    settings = {
      character.error_symbol = "[ ](#${config.colors.base00} bg:#${config.colors.base08})[${sep}](bold #${config.colors.base08})";
      character.success_symbol = "[ ](#${config.colors.base00} bg:#${config.colors.base0A})[${sep}](bold #${config.colors.base0A})";
      cmake.format = starship-color config.colors.base09 "$symbol($version )";
      cmake.version_format = "$major.$minor";
      cmd_duration.format = starship-color config.colors.base07 "took $duration";
      directory.format =
        "[$path$read_only ](bold #${config.colors.base00} bg:#${config.colors.base0B})[${sep}]($bold #${config.colors.base0B})";
      git_branch.format = "[[${sep} $symbol$branch ](#${config.colors.base00} bg:#${config.colors.base0A})]($style)";
      git_status.ahead = "";
      git_status.behind = "";
      git_status.untracked = "";
      git_status.stashed = "";
      git_status.format =
        "([\\[$all_status$ahead_behind\\] ](#${config.colors.base00} bg:#${config.colors.base0A}))[${sep}]($bold #${config.colors.base0A})";
      nix_shell.format = starship-color config.colors.base0D "$symbol(($name))";
      nodejs.format = starship-color config.colors.base09 "$symbol($version)";
      package.format = starship-color config.colors.base08 "$symbol$version";
      purescript.format = starship-color config.colors.base09 "$symbol($version )";
      purescript.version_format = "$major.$minor";
      rust.format = starship-color config.colors.base09 "$symbol($version) ";
      python.format = starship-color config.colors.base09
        "\${symbol}\${pyenv_prefix}(\${version} )(($virtualenv))";
      python.version_format = "$major.$minor";
    };
  };
  programs.tmux = {
    enable = true;
    extraConfig = ''
      bind-key N new-session
      bind-key -T copy-mode-vi v send-keys -X begin-selection
      bind-key -T copy-mode-vi y send-keys -X copy-selection-and-cancel
      set-window-option -g mode-keys vi

      # turn on window titles
      set -g set-titles on

      # set wm window title string
      set -g set-titles-string '#W'

      # automatically set window title
      setw -g automatic-rename on
    '';
  };

  programs.zoxide.enable = true;
  xdg.configFile = {
    "pet".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/pet";
  };

  home.packages = with pkgs; [
    exa
    fd
    pup
    htop
    jc
    jq
    just
    khal
    pet
    plantuml
    ripgrep
    sd
    sshfs
    tealdeer
    wuzz
  ];

  programs.dircolors = {
    enable = true;
    enableFishIntegration = true;
    settings = {
      di = "4;31";
      ln = "32";
      ex = "1;31";
      "*.org" = "36";
      "*.txt" = "36";
      "*.log" = "36";
      "*.md" = "36";
      "*.tex" = "36";
      "*.png" = "33";
      "*.jpg" = "33";
      "*.gif" = "33";
      "*.bmp" = "33";
      "*.hs" = "35";
      "*.nix" = "35";
      "*.dhall" = "35";
      "*.cabal" = "35";
      "*.py" = "35";
      "*.c" = "35";
      "*.pdf" = "34";
      "*.doc" = "34";
      "*.docx" = "34";
      "*.odt" = "34";
      "*.gz" = "4";
      "*.tar" = "4";
      "*.zip" = "4";
      "*.xz" = "4;";
    };
  };

}
