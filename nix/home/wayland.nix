{ config, pkgs, nixpkgs, eww, ... }:

let
  inherit (config) colors;
  background = "#${colors.base00}";
  brite_bg = "#${colors.base01}";
  dim = "#${colors.base04}";
  fg = "#${colors.base05}";
  brite = "#${colors.base07}";

  transition = fmt: color: ''<span color="#${color}"></span>${fmt}'';
in {
  home.packages = [
    pkgs.pulseaudio # needed for avizo - does not actually start pulse audio, thankfully
    pkgs.foot
    pkgs.bc # needed for avizo
    pkgs.wev
    pkgs.wl-clipboard
    eww.packages.x86_64-linux.eww-wayland
    pkgs.blueman
    pkgs.numix-cursor-theme
    # pkgs.gnomeExtensions.snowy
    # pkgs.gnomeExtensions.extension-list
    # pkgs.gnomeExtensions.auto-move-windows
    # pkgs.gnomeExtensions.no-title-bar
  ];
  home.sessionVariables.WLR_NO_HARDWARE_CURSORS = 1;
  dconf.settings."org.gnome.desktop.interface".cursor-theme = "Numix-Cursor";
  programs.waybar = {
    enable = true;
    settings = [{
      layer = "top";
      position = "top";
      height = 14;
      modules-left = [ "sway/workspaces" "sway/mode" ];
      modules-center = [ "sway/window" ];
      modules-right = [
        "pulseaudio"
        "mpd"
        "network"
        "cpu"
        "memory"
        "battery"
        "clock"
        "tray"
      ];
      modules = {
        mpd = {
          format =
            "{stateIcon}{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{title}";
          format-disconnected = "";
          format-stopped = "";
          max-length = 20;
          unknown-tag = "N/A";
          interval = 2;
          consume-icons = { on = " "; };
          random-icons = {
            off = "";
            on = " ";
          };
          repeat-icons = { on = " "; };
          single-icons = { on = "1 "; };
          state-icons = {
            paused = "";
            playing = "";
          };
          on-click = "mpc toggle";
          tooltip-format = "{artist}-{album} {elapsedTime}/{totalTime}";
          tooltip-format-disconnected =
            "{artist}-{album} {elapsedTime}/{totalTime}";
        };
        cpu = {
          format = transition "{usage}% ﬙" colors.base0A;
          tooltip = false;
          states.critical = 50;
          on-click = "${pkgs.foot}/bin/foot ${pkgs.htop}/bin/htop";
        };
        memory = {
          format = transition "{}% " colors.base0B;
          states.critical = 85;
        };
        clock = {
          timezone = "Europe/London";
          format = transition "{:%H:%M}" colors.base0D;
          tooltip-format = ''
            <big>{:%Y %B}</big>
            <tt><small>{calendar}</small></tt>'';
          format-alt = transition "{:%Y-%m-%d}" colors.base0E;
        };
        pulseaudio = {
          format = transition "{volume}% {icon}" colors.base00;
          format-muted = "";
          format-bluetooth = "{volume}% {icon}";
          format-icons.default = [ "" "" ];
          format-icons.headphones = "";
          tooltip-format = "{desc}";
        };
        battery = {
          states.warning = 30;
          states.critical = 15;
          format = transition "{capacity}% {icon}" colors.base0C;
          format-alt = transition "{time}% {icon}" colors.base0C;
          format-icons = [ "" "" "" "" "" ];
          format-charging = transition "{time}% " colors.base0C;
          format-plugged = transition "{capacity}% " colors.base0C;
        };

        network = {
          format-wifi = transition "{signalStrength}% " colors.base0F;
          format-ethernet = transition "" colors.base0A;
          format-linked = transition "{ifname} (No IP) " colors.base0F;
          format-disconnected = transition "Disconnected ⚠" colors.base0F;
          format-alt = transition "{ifname}: {ipaddr}/{cidr}" colors.base0F;
          tooltip-format-wifi = "{essid}";
        };
        "sway/window" = {
          rewrite = {
            "vim (.*)" = "  $1";
            "cat (.*)" = " 🐱 $1";
            "fish (.*)" = " $1";
            "nix-shell -p \\w+ (.*)" = "  $1";
            "(.*)\\.css(.*)" = "$1  $2";
            "(.*)\\.nix(.*)" = "$1  $2";
            "(.*)\\.py(.*)" = "$1  $2";
            "(.*)\\.js(.*)" = "$1  $2";
            "(.*)\\.hs(.*)" = "$1  $2";
            "(.*) — Mozilla Firefox" = " $1";
            "(.*).STFC,RAL,ISIS.*" = "$1 ☢";
            "(.*)Issue #(.*)" = "$1 $2";
            "(.*)Pull Request #(.*)" = "$1 $2";
            "(.*)disorderedmaterials/dissolve(.*)" = "$1⏣$2";
            "(.*) - Vivaldi" = "🎻 $1";
            "(.*) - Rust(.*)" = " $1 $2";
            "(.*) - Google Search(.*)" = " $1 $2";
            "(.*) at DuckDuckGo(.*)" = " $1 $2";
            "(.*) - Stack Overflow(.*)" = " $1 $2";
            "Pull requests ·(.*)" = "$1";
            "(.*) \\| Microsoft Teams" = " $1";
            "(.*) - GNU Emacs at (.*)" = " $1";
            "(.*)COMMIT_EDITMSG(.*)" = "$1$2";
          };
        };
        "sway/workspaces" = {
          disable-scroll = true;
          all-outputs = true;
          format = "{icon}";
          format-icons = {
            "Main" = "";
            "Web" = "⎈";
            "Emacs" = "";
            "Science" = "🧪";
            "Chat" = "";
            "Teams" = "";
            "Zoom" = "🔍";
          };
        };
      };

    }];
    style = ''
      * {
        border: none;
        border-radius: 0;
        font-family: ${config.font-name};
        font-size: ${toString config.font-size};
      }
      window#waybar {
        background: #${colors.base00};
        color: #${colors.base05};
      }
      #workspaces button {
        padding: 0 5px;
        background: #${colors.base00};
        color: #${colors.base04};
      }
      #workspaces button label{
        padding: 0 5px;
        background: #${colors.base00};
        color: #${colors.base03};
      }
      #workspaces button.focused label{
        padding: 0 5px;
        background: #${colors.base00};
        color: #${colors.base06};
      }
      #battery {
        border: 2px;
        color: #${colors.base00};
        background: #${colors.base0D};
      }
      #battery.warning {
        background: #${colors.base09};
      }
      #battery.critical {
        background: #${colors.base08};
      }
      #clock {
        color: #${colors.base00};
        background: #${colors.base0E};
      }
      #memory {
        color: #${colors.base00};
        background: #${colors.base0C};
      }
      #memory {
        color: #${colors.base00};
        background: #${colors.base0C};
      }
      #memory.critical {
        background: #${colors.base08};
      }
      #cpu {
        color: #${colors.base00};
        background: #${colors.base0B};
      }
      #cpu.critical {
        background: #${colors.base08};
      }
      #network {
        color: #${colors.base00};
        background: #${colors.base0A};
      }
      #pulseaudio {
        color: #${colors.base00};
        background: #${colors.base0F};
      }
      #mpd {
        color: #${colors.base00};
        background: #${colors.base0F};
      }
      #tray {
        color: #${colors.base00};
        background: #${colors.base0F};
      }
    '';
  };
  systemd.user.sessionVariables.WLR_NO_HARDWARE_CURSORS = 1;
  services.kanshi.enable = false;
  services.kanshi.profiles = {
    docked.outputs = [
      {
        criteria = "eDP-1";
        status = "disable";
      }
      {
        criteria = "DP-5";
        status = "enable";
        position = "0,0";
      }
      {
        criteria = "DP-6";
        status = "enable";
        position = "1920,0";
      }
    ];
    undocked.outputs = [{
      criteria = "eDP-1";
      status = "enable";
    }];
  };
  services.udiskie.enable = true;
  services.udiskie.tray = "auto";
  systemd.user.services.avizo = {
    Unit.Description = "Avizo display bar";
    Service.ExecStart = "${pkgs.avizo}/bin/avizo-service";
    Install.WantedBy = [ "graphical-session.target" ];
  };
  systemd.user.targets.tray = {
    Unit = {
      Description = "Home Manager System Tray";
      Requires = [ "graphical-session-pre.target" ];
    };
  };
  xdg.configFile = {
    "avizo/config.ini".text = ''
      [default]
      time=0.3
    '';
    "foot/foot.ini".text = ''
      font=${config.font-name}:size=${toString (config.font-size / 2)}
      dpi-aware=yes
      term=xterm-256color

      [colors]
      foreground=${colors.base05}
      background=${colors.base00}
      regular0=${colors.base02}
      regular1=${colors.base09}
      regular2=${colors.base0A}
      regular3=${colors.base0B}
      regular4=${colors.base0C}
      regular5=${colors.base0D}
      regular6=${colors.base0E}
      regular7=${colors.base07}
    '';
    "eww/eww.yuck".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/eww/eww.yuck";
    "eww/weather.yuck".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/eww/weather.yuck";
    "eww/eww.scss".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/eww/eww.scss";
    "eww/_colors.scss".text = ''
      $color00: #${colors.base00};
      $color01: #${colors.base01};
      $color02: #${colors.base02};
      $color03: #${colors.base03};
      $color04: #${colors.base04};
      $color05: #${colors.base05};
      $color06: #${colors.base06};
      $color07: #${colors.base07};
      $color08: #${colors.base08};
      $color09: #${colors.base09};
      $color0A: #${colors.base0A};
      $color0B: #${colors.base0B};
      $color0C: #${colors.base0C};
      $color0D: #${colors.base0D};
      $color0E: #${colors.base0E};
      $color0F: #${colors.base0F};
    '';
  };
  wayland.windowManager.sway = {
    enable = true;
    config = {
      modifier = "Mod4";
      assigns.Main =
        [ { instance = "xterm"; } { class = "URxvt"; } { app_id = "foot"; } ];
      assigns.Web = [
        { instance = "Navigator"; }
        { window_role = "browser"; }
        { app_id = "firefox"; }
      ];
      assigns.Emacs = [{ class = "Emacs"; }];
      assigns.Science =
        [ { instance = "dissolve-gui"; } { class = "Matplotlib"; } ];
      assigns.Game = [ { class = "minecraft"; } { instance = "Minecraft"; } ];
      assigns.Zoom = [{ title = "Zoom - Licensed Account"; }];
      colors.background = "#${colors.base00}";
      colors.focused = {
        background = "#${colors.base00}";
        border = "#${colors.base01}";
        childBorder = "#${colors.base01}";
        text = "#${colors.base06}";
        indicator = "#${colors.base06}";
      };
      colors.focusedInactive = {
        background = "#${colors.base00}";
        border = "#${colors.base00}";
        childBorder = "#${colors.base00}";
        text = "#${colors.base04}";
        indicator = "#${colors.base04}";
      };
      colors.unfocused = {
        background = "#${colors.base00}";
        border = "#${colors.base00}";
        childBorder = "#${colors.base00}";
        text = "#${colors.base04}";
        indicator = "#${colors.base04}";
      };
      floating.titlebar = true;
      floating.criteria = [
        { title = "Steam - Update News"; }
        { class = "Pavucontrol"; }
        { title = "Nasus Picker"; }
        { title = "Sharing Indicator"; }
      ];
      output.Virtual-1 = {
        resolution = "3840x2400";
        scale = "2";
      };
      input."type:keyboard" = {
        xkb_layout = "gb";
        xkb_options = "ctrl:nocaps";
      };
      keybindings = {
        "Mod4+Shift+q" = "kill";
        "Mod4+1" = "workspace Main";
        "Mod4+2" = "workspace Web";
        "Mod4+3" = "workspace Emacs";
        "Mod4+4" = "workspace Science";
        "Mod4+5" = "workspace Chat";
        "Mod4+6" = "workspace Teams";
        "Mod4+7" = "workspace Zoom";
        "Mod4+8" = "workspace number 8";
        "Mod4+9" = "workspace number 9";
        "Mod4+Shift+1" = "move container to workspace Main";
        "Mod4+Shift+2" = "move container to workspace Web";
        "Mod4+Shift+3" = "move container to workspace Emacs";
        "Mod4+Shift+4" = "move container to workspace Science";
        "Mod4+Shift+5" = "move container to workspace Chat";
        "Mod4+Shift+6" = "move container to workspace Teams";
        "Mod4+Shift+7" = "move container to workspace Zoom";
        "Mod4+Shift+8" = "move container to workspace number 8";
        "Mod4+Shift+9" = "move container to workspace number 9";
        "Mod4+e" = "move workspace to output right";
        "Mod4+w" = "move workspace to output left";

        "Print" = "exec ${pkgs.grim}/bin/grim";
        "Mod4+Print" =
          ''exec ${pkgs.grim}/bin/grim -g "`${pkgs.slurp}/bin/slurp`"'';

        "Mod4+numbersign" = "exec ${pkgs.dunst}/bin/dunstctl close";
        "Mod4+Shift+numbersign" = "exec ${pkgs.dunst}/bin/dunstctl close-all";

        "Mod4+Return" = "exec ${pkgs.foot}/bin/foot";
        "Mod4+Mod1+e" = "exec ~/.nix-profile/bin/emacsclient -c";

        "Mod4+a" = "exec ${pkgs.wf-recorder}/bin/wf-recorder";
        "Mod4+Shift+a" = "exec pkill --signal SIGINT wf-recorder";

        "Mod4+b" = "splith";
        "Mod4+v" = "splitv";
        "Mod4+f" = "floating toggle";
        "Mod4+s" = "layout toggle splitv tabbed";
        "Mod4+Shift+s" = "fullscreen toggle";
        "Mod4+t" = "output eDP-1 toggle";
        "Mod4+n" = "exec fish ${./window_switcher.fish}";

        "Mod4+p" = "exec ${pkgs.rofi}/bin/rofi -i -show pass";
        "Mod4+i" = "exec ${pkgs.rofi}/bin/rofi -i -show bookmarks";
        "Mod4+Shift+z" =
          "exec swaylock --image ~/Downloads/stfc.png -c '#${colors.base00}' --text-color '#${colors.base05}' --inside-ver-color '#${colors.base0B}' --inside-wrong-color '#${colors.base09}' --key-hl-color '#${colors.base0D}' --ring-color '#${colors.base0E}' -r --indicator-radius 400 --indicator-thickness 50";

        "Mod4+d" =
          "exec PATH=~/.nix-profile/bin ${pkgs.rofi}/bin/rofi -show-icons -i -show run";
        "Mod4+Shift+d" =
          "exec PATH=~/.nix-profile/bin ${pkgs.rofi}/bin/rofi -show-icons -modi drun -show drun";

        "Mod4+g" = ''
          exec swaymsg -t command workspace `swaymsg -t get_workspaces | jq -r ".[] .name" | rofi -i -dmenu`'';
        "Mod4+Shift+g" = ''
          exec swaymsg -t command move container to workspace `swaymsg -t get_workspaces | jq -r ".[] .name" | rofi -i -dmenu`'';
        "Mod4+r" = "mode resize";
        "Mod4+Left" = "focus left";
        "Mod4+Right" = "focus right";
        "Mod4+Up" = "focus up";
        "Mod4+Down" = "focus down";
        "Mod4+Shift+Left" = "move left";
        "Mod4+Shift+Right" = "move right";
        "Mod4+Shift+Up" = "move up";
        "Mod4+Shift+Down" = "move down";
        "Mod4+Shift+c" = "reload";
        "Mod4+Shift+e" =
          "exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'";
        "XF86AudioRaiseVolume" = "exec ${pkgs.avizo}/bin/volumectl raise";
        "XF86AudioLowerVolume" = "exec ${pkgs.avizo}/bin/volumectl lower";
        "XF86AudioMute" = "exec ${pkgs.avizo}/bin/volumectl mute";
        "XF86MonBrightnessDown" = "exec ${pkgs.avizo}/bin/lightctl lower";
        "XF86MonBrightnessUp" = "exec ${pkgs.avizo}/bin/lightctl raise";

      };
      modes = {
        resize = {
          Down = "resize grow height 10 px";
          Escape = "mode default";
          Left = "resize shrink width 10 px";
          Return = "mode default";
          Right = "resize grow width 10 px";
          Up = "resize shrink height 10 px";
          h = "resize shrink width 10 px";
          j = "resize grow height 10 px";
          k = "resize shrink height 10 px";
          l = "resize grow width 10 px";
        };
      };
      bars = [{ command = "${pkgs.waybar}/bin/waybar"; }];
      seat."*" = { xcursor_theme = "Numix-Cursor 24"; };
      startup = [
        # {
        #   command =
        #     "${eww.packages.x86_64-linux.eww-wayland}/bin/eww open status";
        # }
        {
          command =
            "${eww.packages.x86_64-linux.eww-wayland}/bin/eww open example";
        }
        { command = "${pkgs.keychain}/bin/keychain --eval --quiet id_rsa"; }
        { command = "${pkgs.pass}/bin/pass microsoft"; }
      ];
    };
  };
}
