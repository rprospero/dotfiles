{
  # Needed for Slack
  allowUnfree = true;

  allowUnfreePredicate = (pkg: true);
  # Needed for Semantic
  allowBroken = true;
  permittedInsecurePackages = [ "libgit2-0.27.10" ];
}
