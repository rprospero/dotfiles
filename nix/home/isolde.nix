{ pkgs, ... }:

{
  accounts.email.accounts.Personal.primary = true;
  home.packages = [
    pkgs.gallery-dl
    pkgs.texlive.combined.scheme-full
    pkgs.nyxt
    pkgs.youtube-dl
    # pkgs.libsForQt5.vlc

  ];
  programs.autorandr.enable = true;
  programs.autorandr.profiles.rdp = {
    fingerprint.rdp0 = "CONNECTED-BUT-EDID-UNAVAILABLE--rdp0";
    config.rdp0 = {
      enable = true;
      dpi = 210;
    };
  };
  programs.autorandr.profiles.tv = {
    fingerprint.tv =
      "HDMI-0 00ffffffffffff001e6d010001010101011a010380a05a780aee91a3544c99260f5054a1080031404540614071408180010101010101023a801871382d40582c4500a05a0000001e662150b051001b3040703600a05a0000001e000000fd003a3e1e5310000a202020202020000000fc004c472054560a20202020202020013f020322f14e109f0413051403021220212215012615075009570767030c001200801e011d8018711c1620582c250020c23100009e011d007251d01e206e28550020c23100001e023a801871382d40582c4500a05a0000001e011d00bc52d01e20b8285540c48e2100001e00000000000000000000000000000000000000000023";
    config.tv = {
      enable = true;
      dpi = 210;
    };
  };
}
