{ pkgs, ... }:

let
  message = txt: {
    Unit = { Description = "Remind myself to ${txt}"; };
    Service = {
      Type = "oneshot";
      ExecStart =
        ''${pkgs.libnotify}/bin/notify-send --category=alarm "${txt}"'';
    };
  };
  timer = txt: {
    Unit = { Description = "Remind myself on time"; };
    Timer = { OnCalendar = txt; };
    Install = { WantedBy = [ "graphical-session.target" ]; };
  };

in {
  # systemd.user.services.breaktime = message "Take a break";
  # systemd.user.timers.breaktime = timer "Mon,Tue,Wed,Thu,Fri 9,10,13,14,15,16:50:00";
  systemd.user.services.mainproject = message "Work on main project";
  systemd.user.timers.mainproject = timer "Mon,Tue,Wed,Thu,Fri 12:30,15:30";
  systemd.user.services.secondaryproject = message "Code Review";
  systemd.user.timers.secondaryproject =
    timer "Mon,Tue,Wed,Thu,Fri 10,17:00:00";
  systemd.user.services.mantidproject = message "Mantid";
  systemd.user.timers.mantidproject = timer "Mon,Tue,Wed,Thu,Fri 14:00";
  systemd.user.services.pizza = message "Order dinner";
  systemd.user.timers.pizza = timer "Mon,Tue,Wed,Thu,Fri 17:30:00";
  systemd.user.services.lunch = message "Lunch break";
  systemd.user.timers.lunch = timer "Mon,Tue,Wed,Thu,Fri 11:30:00";
  systemd.user.services.stopworking = message "Stop Working";
  systemd.user.timers.stopworking = timer "Mon,Tue,Wed,Thu,Fri 18:00:00";
}
