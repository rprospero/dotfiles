{ pkgs, ... }:

{
  programs.gpg.enable = true;
  programs.gpg.settings.local-user = "rprospero@gmail.com";
  programs.gpg.settings.default-key = "rprospero@gmail.com";

  programs.keychain = {
    enable = true;
    enableFishIntegration = true;
    keys = [ "id_rsa" ];
  };

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 2678400;
    maxCacheTtl = 2678400;
    enableSshSupport = false;
    pinentryFlavor = "gtk2";
    extraConfig = ''
      allow-emacs-pinentry
      allow-loopback-pinentry
    '';
  };
}
