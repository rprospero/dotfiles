{ pkgs ? import <nixpkgs>, ... }:

let
  py = pkgs.python3.withPackages (pkgs: [ pkgs.msal ]);

  # pkgs.python3Packages.buildPythonPackage {
in pkgs.stdenv.mkDerivation {
  name = "M365-imap";
  src = pkgs.fetchFromGitHub {
    owner = "UvA-FNWI";
    repo = "M365-IMAP";
    rev = "main";
    sha256 = "MpYmz175ZODvOQ+yxUt3S3iXN1EfUFkcsIuKa68cG7A=";
  };
  buildInputs = [ py ];
  installPhase = ''
    chmod +x *.py
    mkdir $out
    cp -r * $out
    cat <(echo "#!${py}/bin/python") refresh_token.py > $out/refresh_token.py
    cat <(echo "#!${py}/bin/python") get_token.py > $out/get_token.py
    head -n3 config.py > $out/config.py
    echo "RefreshTokenFileName = '/home/adam/Code/M365-IMAP/imap_smtp_refresh_token'" >> $out/config.py
    echo "AccessTokenFileName = '/home/adam/Code/M365-IMAP/imap_smtp_access_token'" >> $out/config.py
  '';
}
