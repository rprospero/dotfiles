{ pkgs, ... }:

let

  unstable = import (pkgs.fetchFromGitHub {
    owner = "nixos";
    repo = "nixpkgs";
    rev = "19.03-beta";
    sha256 = "1wr6dzy99rfx8s399zjjjcffppsbarxl2960wgb0xjzr7v65pikz";
  }) { };

in {
  imports = [
    ./common.nix
    ./personal_science.nix
    ./local_mail.nix
    ./graphical.nix
    ./x11.nix
  ];
  accounts.email.accounts.Personal.primary = true;
  home.packages = [
    pkgs.distcc
    pkgs.dosbox
    pkgs.minecraft
    pkgs.texlive.combined.scheme-full
  ];
  # programs.emacs.package = unstable.emacs;
}
