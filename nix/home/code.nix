{ pkgs, ... }:

{
  home.packages = with pkgs; [
    cargo
    ccls
    cppcheck
    ctags
    dhall
    hlint
    # qtcreator
  ];
}
