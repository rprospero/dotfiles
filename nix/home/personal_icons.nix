{ pkgs, lib, config, ... }:

let
  foreground = config.colors.base05;
  background = config.colors.base00;
  icons = pkgs.stdenv.mkDerivation rec {
    pname = "personal-icons";
    version = "0.1";
    phases = [ "installPhase" ];
    # dontFetch = true;
    # dontBuild = true;
    installPhase = ''
      mkdir -p $out/share/icons/personal
      mkdir -p /tmp/personal-icons
      cp -R ${pkgs.gnome-themes-extra}/share/icons/HighContrast/* /tmp/personal-icons
      chmod -R +w /tmp/personal-icons
      for i in `find  /tmp/personal-icons/ -name "xchat-gnome.png"`; do rm $i; done
      for i in `find /tmp/personal-icons/ -name "*.png"`; do ${pkgs.imagemagick}/bin/convert $i -fuzz 30% -fill "#${foreground}" -opaque white -fill "#${background}" -opaque black $i; done
      cp -R /tmp/personal-icons/* $out/share/icons/personal/
    '';
  };

in {
  icon-path = "${icons}/share/icons/personal/48x48";
  home.packages = [ icons ];
}
