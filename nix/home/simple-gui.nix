{ config, pkgs, ... }:

let
  inherit (config) colors;
  fixed_font =
    [ "xft:${config.font-name}:pixelsize=20:antialias=true:hinting=true" ];
  myAspell = pkgs.aspellWithDicts (ps: with ps; [ en en-science ]);

in {

  imports = [ ./dunst.nix ./rofi.nix ./firefox.nix ];

  programs.noti.enable = true;
  programs.noti.settings.keybase.conversation = "rprospero";
  programs.noti.settings.pushover.userKey =
    (import ../../secrets/tokens.nix).pushoverKey;
  programs.noti.settings.pushover.apiToken =
    (import ../../secrets/tokens.nix).pushoverToken;
  home.sessionVariables.NOTI_DEFAULT = "banner pushover";

  programs.feh.enable = true;

  programs.zathura = {
    enable = true;
    options = {
      default-bg = "#${colors.base00}";
      default-fg = "#${colors.base04}";

      statusbar-fg = "#${colors.base03}";
      statusbar-bg = "#${colors.base01}";

      inputbar-bg = "#${colors.base01}";
      inputbar-fg = "#${colors.base06}";

      notification-error-bg = "#${colors.base08}";
      notification-error-fg = "#${colors.base00}";

      notification-warning-bg = "#${colors.base08}";
      notification-warning-fg = "#${colors.base00}";

      highlight-color = "#${colors.base0A}";
      highlight-active-color = "#${colors.base0D}";

      index-active-bg = "#${colors.base0D}";

      completion-highlight-fg = "#${colors.base02}";
      completion-highlight-bg = "#${colors.base0C}";

      completion-bg = "#${colors.base02}";
      completion-fg = "#${colors.base0C}";

      notification-bg = "#${colors.base0B}";
      notification-fg = "#${colors.base00}";

      recolor-lightcolor = "#${colors.base00}";
      recolor-darkcolor = "#${colors.base05}";
      recolor = true;
      recolor-keephue = false;
    };
  };

  home.packages = with pkgs; [
    gnuplot
    imv
    mlterm
    pcmanfm
    rxvt_unicode
    rxvt_unicode.terminfo
  ];

  xresources.properties = {
    "urxvt*skipScroll" = true;
    "urxvt*scrollBar" = false;
    "*foreground" = "#" + colors.base05;
    "*background" = "#" + colors.base00;
    "*cursorColor" = "#" + colors.base05;
    "*color0" = "#" + colors.base00;
    "*color1" = "#" + colors.base08;
    "*color2" = "#" + colors.base0B;
    "*color3" = "#" + colors.base0A;
    "*color4" = "#" + colors.base0D;
    "*color5" = "#" + colors.base0E;
    "*color6" = "#" + colors.base0C;
    "*color7" = "#" + colors.base05;
    "*color8" = "#" + colors.base03;
    "*color9" = "#" + colors.base09;
    "*color10" = "#" + colors.base01;
    "*color11" = "#" + colors.base02;
    "*color12" = "#" + colors.base04;
    "*color13" = "#" + colors.base06;
    "*color14" = "#" + colors.base0F;
    "*color15" = "#" + colors.base05;
    "URxvt.font" = fixed_font;
  };

}
