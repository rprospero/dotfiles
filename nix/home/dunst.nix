{ config, pkgs, ... }:

let inherit (config) colors;

in {
  services.dunst = {
    enable = true;
    settings = {
      global = {
        follow = "keyboard";
        format = "<b>%s</b>\\n%b";
        geometry = "200x5-0+0";
        markup = "full";
        icon_position = "left";
        word_wrap = "yes";
        font = "${config.font-name} ${toString config.font-size}";
      };
      shortcuts = {
        history = "mod4+grave";
        context = "mod4+shift+notsign";
        close = "mod4+numbersign";
        close_all = "mod4+shift+numbersign";
      };
      urgency_low = {
        background = "#${colors.base00}";
        foreground = "#${colors.base03}";
      };
      urgency_normal = {
        background = "#${colors.base00}";
        foreground = "#${colors.base05}";
      };
      urgency_critical = {
        background = "#${colors.base09}";
        foreground = "#${colors.base00}";
      };
      alarm = {
        category = "alarm";
        background = "#${colors.base00}";
        foreground = "#${colors.base05}";
        timeout = 0;
        new_icon = "${config.icon-path}/status/alarm.png";
      };

      jabber_message = {
        appname = "Emacs";
        summary = "(PM)*";
        timeout = 0;
        urgency = "critical";
      };
      azure_completed = {
        appname = "Azure Notify";
        summary = "completed";
        timeout = 0;
        new_icon = "${config.icon-path}/actions/media-playback-stop.png";
      };
      azure_working = {
        appname = "Azure Notify";
        summary = "inProgress";
        timeout = 0;
        new_icon = "${config.icon-path}/actions/media-playback-start.png";
      };
      noti = {
        body = "Done!";
        timeout = 0;
        new_icon = "${config.icon-path}/stock/gtk-yes.png";
      };
      noti_fail = {
        body = "exit status*";
        timeout = 0;
        background = "#${colors.base08}";
        foreground = "#${colors.base00}";
        new_icon = "${config.icon-path}/stock/gtk-no.png";
      };
      travis_started = {
        appname = "notify-send";
        body = "*started*";
        urgency = "low";
      };
      travis_passed = {
        appname = "notify-send";
        body = "*passed*";
        urgency = "low";
        foreground = "#${colors.base05}";
      };
      travis_failed = {
        appname = "notify-send";
        body = "*failed*";
        urgency = "critical";
        timeout = 0;
        background = "#${colors.base0C}";
        foreground = "#${colors.base00}";
      };
    };
  };

}
