{ config, pkgs, lib, ... }:
let

  inherit (config) colors;

in {

  gtk = {
    iconTheme.package = pkgs.elementary-xfce-icon-theme;
    iconTheme.name = "Elementary";
    theme.name = "Adwaita";
    theme.package = pkgs.gnome-themes-extra;
  };

  xdg.configFile = {
    "gtk-3.0/gtk.css".text = ''
      grid label, button {
        font-family:  ${config.font-name};
        font-size:  ${toString config.font-size}pt;
      }
      .status-bar {
        font-size: calc(0.5 * ${toString config.font-size}pt);
      }
      .title {
        font-size: 200%;
      }
      *:disabled, *:disabled label {
        color: #${colors.base03};
        background-color: #${colors.base0C};
      }
      *:selected, *:selected label {
        color: #${colors.base00};
        background-color: #${colors.base0C};
      }
      *:focus, *:focus label {
        color: #${colors.base0B};
        background-color: #${colors.base00};
      }
      separator {
        background-color: #${colors.base04};
      }
      switch:checked {
        color: #${colors.base00};
        background-color: #${colors.base07};
      }
      .titlebar {
        background-image: none;
        border-style: none;
        box-shadow: none;
      }
      button, button label, .image-button, button image {
        background-color: #${colors.base0D};
        color: #${colors.base00};
        border-color: #${colors.base04};
        background-image: none;
      }
      window, row, treeview, .pathbar {
        background-color: #${colors.base00};
        color: #${colors.base05};
        border-color: #${colors.base04};
      }
      headerbar {
        color: #${colors.base07};
        background-color: #${colors.base01};
      }
    '';
  };
}
