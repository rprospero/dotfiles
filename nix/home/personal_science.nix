{ pkgs, lib, ... }: {
  imports = [ ./color-settings.nix ];
  font-name  = "Hack Nerd Font Mono";
  colors = {
    base00 = "1a052e";
    base01 = "351539";
    base02 = "4e2444";
    base03 = "945f65";
    base04 = "dfbea6";
    base05 = "f0d8bc";
    base06 = "fff2d4";
    base07 = "ffffef";
    base08 = "ff8754";
    base09 = "d6b104";
    base0A = "64ca4b";
    base0B = "00d4b2";
    base0C = "00d1ff";
    base0D = "00bcff";
    base0E = "e392ff";
    base0F = "ff6fb7";
  };
}
