{ config, pkgs, lib, ... }:
let

  inherit (config) colors;
  nerdfont = name: sha256:
    pkgs.fetchzip {
      inherit sha256;
      name = "${name}-nerd-font";
      url =
        "https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/${name}.zip";
      postFetch = ''
        unzip $downloadedFile
        install -m444 -Dt $out/share/fonts/truetype {*.ttf,*.otf}
      '';
    };

in {
  imports = [
    ./fonts.nix
    ./simple-gui.nix
    ./gtk.nix
    ./personal_icons.nix
    ./qt-theme.nix
  ];
  services.kbfs.enable = true;
  services.keybase.enable = true;
  services.keynav.enable = true;

  home.file = {
    "setroot.sh" = {
      executable = true;
      text = ''
        #!/usr/bin/env sh
        hsetroot -solid '#${colors.base00}'
        xsetroot -cursor_name left_ptr -fg '#${colors.base00}' -bg '#${colors.base05}'
      '';
    };
  };

  xdg.configFile."stylus.json".text =
    pkgs.lib.generators.toJSON { } (import ./stylus.nix { inherit colors; });

  xdg.configFile."theme/colours.css".text = ''
    body {
        --colorFg:#${colors.base05};
        --colorFgAlpha:#${colors.base05}1a;
        --colorFgIntense:#${colors.base06};
        --colorFgFaded:#${colors.base04};
        --colorFgFadedMore:#${colors.base03};
        --colorFgFadedMost:#${colors.base02};
        --colorBg:#${colors.base00};
        --colorBgAlpha:#${colors.base00}e6;
        --colorBgAlphaHeavy:#${colors.base00}a6;
        --colorBgAlphaHeavier:#${colors.base00}40;
        --colorBgAlphaBlur:#${colors.base00}f2;
        --colorBgDark:#${colors.base00};
        --colorBgDarker:#${colors.base00};
        --colorBgLight:#${colors.base00};
        --colorBgLighter:#${colors.base01};
        --colorBgLightIntense:#${colors.base01};
        --colorBgIntense:#${colors.base01};
        --colorBgIntenser:#${colors.base01};
        --colorBgIntserAlpha:#fbfbfbf2;
        --colorBgInverse:#f1f1f1;
        --colorBgInverser:#e1e1e1;
        --colorBgFaded:#e7e7e7;
        --backgroundBlur:blur(0px);
        --colorHighlightBg:#${colors.base0D};
        --colorHighlightBgUnfocused:#e7e7e7;
        --colorHighlightBgAlpha:#${colors.base0D}1a;
        --colorHighlightBgDark:#${colors.base0D};
        --colorHighlightFg:#ffffff;
        --colorHighlightFgAlpha:#ffffff80;
        --colorHighlightFgAlphaHeavy:#ffffff40;
        --colorAccentBg:#${colors.base00};
        --colorAccentBgAlpha:#${colors.base00}66;
        --colorAccentBgAlphaHeavy:#${colors.base00}40;
        --colorAccentBgDark:#${colors.base00};
        --colorAccentBgDarker:#${colors.base00};
        --colorAccentBgFaded:#${colors.base00};
        --colorAccentBgFadedMore:#${colors.base00};
        --colorAccentBgFadedMost:#${colors.base00};
        --colorAccentBorder:#${colors.base00};
        --colorAccentBorderDark:#${colors.base00};
        --colorAccentFg:#${colors.base05};
        --colorAccentFgFaded:#c6c6c6;
        --colorAccentFgAlpha:#${colors.base05}40;
        --colorBorder:#cdcdcd;
        --colorBorderDisabled:#e0e0e0;
        --colorBorderSubtle:#e3e3e3;
        --colorBorderIntense:#b9b9b9;
        --colorSuccessBg:#${colors.base0C};
        --colorSuccessBgAlpha:#${colors.base0C}1a;
        --colorSuccessFg:#${colors.base00};
        --colorWarningBg:#${colors.base0B};
        --colorWarningBgAlpha:#${colors.base0B}1a;
        --colorWarningFg:#${colors.base00};
        --colorErrorBg:#${colors.base08};
        --colorErrorBgAlpha:#${colors.base08}1a;
        --colorErrorFg:#${colors.base00};
        --radiusRound:100px;
        --radiusRounded:2px;
        --radiusRoundedLess:2px;
        --radius:4px;
        --radiusHalf:calc(var(--radius) * 0.5);
    }
  '';

  home.keyboard.options = [ "ctrl:nocaps" ];
  home.packages = with pkgs; [
    anki
    baobab
    gimp
    gnome2.gnome_icon_theme
    gnome-themes-extra
    libreoffice
    vivaldi
    zathura
  ];
}
