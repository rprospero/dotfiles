{ config, pkgs, ... }:

let
  inherit (config) colors;
  myAspell = pkgs.aspellWithDicts (ps: with ps; [ en en-science ]);

in {
  imports = [
    ./emacs.nix
    ./mail.nix
    ./code.nix
    ./git.nix
    ./gpg.nix
    ./pretty-cli.nix
    ./meta.nix
  ];
  programs.bat.enable = true;
  programs.browserpass.enable = true;
  programs.info.enable = true;
  programs.ssh = import ./ssh.nix;
  programs.password-store.enable = true;
  programs.password-store.package =
    pkgs.pass.withExtensions (ext: [ ext.pass-genphrase ]);
  qt.enable = true;
  qt.platformTheme = "gtk";

  home.file = {
    ".emacs.d/snippets".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/snippets";
    ".zshrc".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/zshrc";
    ".ldaprc".source = config.lib.file.mkOutOfStoreSymlink
      "${config.home.homeDirectory}/Code/dotfiles/ldaprc";
    ".gntrc".text = pkgs.lib.generators.toINI { } {
      colorpairs = {
        text = "red;black";
        highlight = "red;black";
        title = "black;red";
      };
    };
  };

  home.packages = with pkgs; [
    myAspell
    # base16-builder
    basex
    # dhall-text
    entr
    exiftool
    fzf
    gnutls
    graphviz
    jre
    languagetool
    ledger
    libnotify
    links2
    lsof
    lxappearance
    openssh
    pandoc
    pdftk
    proselint
    qrencode
    libsForQt5.qtstyleplugins
    sqlite
    unzip
    vdirsyncer
    weechat
    wkhtmltopdf
    zip
    zotero
  ];

  home.keyboard.layout = "gb";
  home.sessionVariables = {
    FONTCONFIG_FILE = "/home/adam/.config/fontconfig/conf.d/10-hm-fonts.conf";
    QT_QPA_PLATFORMTHEME = "qt5ct";
    TERMINFO_DIRS = "~/.nix-profile/share/terminfo/";
  };

  services.lorri.enable = true;
}
