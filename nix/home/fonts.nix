{ pkgs, lib, ... }:

let

  startrek = pkgs.stdenv.mkDerivation {

    name = "star-trek=fonts";

    src = pkgs.fetchFromGitHub {
      owner = "wrstone";
      repo = "fonts-startrek";
      rev = "master";
      sha256 = "J+jHlO41r9Ewgn40zHaKRDlnILXrOXEPnf+uC6/fYtI=";
    };

    installPhase = ''
      mkdir -p $out/share/fonts/truetype
      cp Swiss*.ttf $out/share/fonts/truetype
    '';
  };

in {
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    cabin
    comic-relief
    dejavu_fonts
    emacs-all-the-icons-fonts
    fira
    font-manager
    iosevka
    liberation-sans-narrow
    (nerdfonts.override { fonts = [ "Monoid" "Hack" ]; })
    startrek
    symbola
  ];
}
