{ pkgs, ... }:

{
  imports = [
    ./common.nix
    # ./local_mail.nix 
    ./graphical.nix
    ./x11.nix
  ];
  accounts.email.accounts.Work.primary = true;
  home.packages = [
    # pkgs.virtualboxExtpack
    pkgs.distcc
    pkgs.texlive.combined.scheme-full
  ];
  home.sessionVariables.LOCALE_ARCHIVE = "/usr/lib/locale/locale-archive";
  targets.genericLinux.enable = true;
  xresources.properties."Xft.dpi" = 331;
}
