{ pkgs, statix, ... }:

{
  # programs.home-manager.enable = true;
  # programs.home-manager.path =
  #   "https://github.com/rycee/home-manager/archive/master.tar.gz";
  nixpkgs.config = import ./nixpkgs-config.nix;
  nixpkgs.overlays = [ (self: super: { nix = self.nixFlakes; }) ];
  home.packages = with pkgs; [
    manix
    nix-prefetch-git
    nix-prefetch-github
    nix-review
    nixfmt
    rnix-lsp
    statix.defaultPackage.x86_64-linux
    (pkgs.writeShellScriptBin "nixFlakes" ''
      exec ${pkgs.nixFlakes}/bin/nix --experimental-features "nix-command flakes" "$@"
    '')
  ];
}
