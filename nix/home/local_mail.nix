{ pkgs, ... }:

let secrets = import ../../secrets/tokens.nix;

in {
  accounts.email.accounts = {
    Personal.notmuch.enable = true;
    Personal.offlineimap.enable = true;
    Personal.offlineimap.postSyncHookCommand =
      "/home/adam/.nix-profile/bin/notmuch new && /home/adam/.nix-profile/bin/afew -t -n";
    Personal.offlineimap.extraConfig.remote.ssl_version = "tls1_2";
    Professional.notmuch.enable = true;
    Professional.offlineimap.enable = true;
    Professional.offlineimap.postSyncHookCommand =
      "/home/adam/.nix-profile/bin/notmuch new && /home/adam/.nix-profile/bin/afew -t -n";
    Professional.offlineimap.extraConfig.remote.ssl_version = "tls1_2";
    Work.notmuch.enable = true;
    Work.offlineimap.enable = true;
    Work.offlineimap.extraConfig.remote = {
      auth_mechanisms = "XOAUTH2";
      oauth2_request_url =
        "https://login.microsoftonline.com/common/oauth2/v2.0/token";
      oauth2_client_id = secrets.work_email.client_id;
      oauth2_client_secret = secrets.work_email.client_secret;
      oauth2_refresh_token = secrets.work_email.refresh_token;
      ssl = true;
    };
  };
  home.packages = with pkgs; [ muchsync ];
  programs.notmuch.enable = true;
  programs.notmuch.new.ignore = [ "/nnmaildir/" "/nnmaildor/" ];
  programs.afew.enable = false;
  programs.afew.extraConfig = ''
    [SpamFilter]
    [Filter.0]
    tags = +science
    query = from:neutronsources.org
    [Filter.1]
    tags = +promotional;+travel
    query = from:secretescapes.com or from:gogreentaxisltd.co.uk or from:oxfordkey.co.uk or from:oxfordbus.co.uk
    [Filter.2]
    tags = +promotional
    query = from:petplanet
    [Filter.3]
    tags = +experiment
    query = from:isisuo@stfc.ac.uk subject:scheduled

    [Filter.4]
    tags = +sasview;
    query = folder:/SASView/

    [Filter.7]
    tags = +geniescans;
    query = from:travis-ci.org subject:PyGenie
    [Filter.8]
    tags = +geniescans;
    query = from:codacy subject:Scans

    [Filter.9]
    tags = +mantid;
    query = from:mantidproject.org

    [Filter.10]
    tags = +science
    query = folder:/Science/

    [Filter.11]
    tags = +promotional;
    query = folder:/Promotional/
    [Filter.12]
    tags = +rifftrax;
    query = folder:/Rifftrax/
    [Filter.13]
    tags = +food;
    query = folder:/Food/
    [Filter.14]
    tags = +games;
    query = folder:/Games/
    [Filter.15]
    tags = +RPG
    query = folder:/RPG/

    [Filter.27]
    tags = +paperspam;
    query = from:ResearchGate
    [Filter.28]
    tags = +paperspam;
    query = from:Academia.edu or from:academia-mail.com
    [Filter.29]
    tags = +paperspam;
    query = from:scholarcitations
    [Filter.30]
    tags = +paperspam;
    query = from:mendeley
    [Filter.32]
    tags = +promotional;+charity;
    query = folder:/Charity/
    [Filter.33]
    tags = +promotional;
    query = from:PetPlanet.co.uk or from:"Powerhouse Fitness" or from:IMDB.com
    [Filter.34]
    tags = +promotional;+games;
    query = "(from:epicgames.com or from:humblebundle or from:failbettergames.com)"
    [Filter.35]
    tags = +promotional;
    query = "(from:discover.com or from:cases.com or from:umrelief)"

    [Filter.40]
    tags = +nix
    query = folder:/Nixos/

    [Filter.100]
    tags = +social
    query = folder:/Community/
    [ArchiveSentMailsFilter]
    [InboxFilter]
  '';
  programs.offlineimap.enable = true;
  programs.offlineimap.pythonFile = ''
    import subprocess

    def get_pass(service, cmd):
        return subprocess.check_output(cmd, ).strip()
  '';

  systemd.user.services.offlineimap = {
    Install.WantedBy = [ "default.target" ];
    Unit = { Description = "Offline Imap Sync"; };
    Service = {
      Type = "oneshot";
      ExecStart = "${pkgs.offlineimap}/bin/offlineimap";
    };
  };
  systemd.user.timers.offlineimap = {
    Install.WantedBy = [ "default.target" ];
    Unit.Description = "Offline Imap Sync Timer";
    Timer = {
      OnUnitInactiveSec = "5 min";
      Unit = "offlineimap.service";
    };
  };
}
