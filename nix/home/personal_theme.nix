{ pkgs, lib, ... }: {
  imports = [ ./color-settings.nix ];
  colors = {
    base00 = "1c433c";
    base01 = "4c775d";
    base02 = "6EB099";
    base03 = "8DCDB3";
    base04 = "70b39c";
    base05 = "91dbbe";
    base06 = "36665A";
    base07 = "befcda";
    base08 = "8e5b5e";
    base09 = "f7a8a5";
    base0A = "e8cbba";
    base0B = "fcd88a";
    base0C = "b4c8a1";
    base0D = "6ec0ff";
    base0E = "bd94ca";
    base0F = "8e725b";
  };
}
