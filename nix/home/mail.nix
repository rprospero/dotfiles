{ pkgs, ... }:

let fixer = import ./outlook.nix {inherit pkgs;};

in {
  accounts.email.accounts = {
    Personal = {
      address = "rprospero@gmail.com";
      flavor = "gmail.com";
      realName = "Adam Washington";
      passwordCommand = "${pkgs.pass}/bin/pass personal_email";
      msmtp.enable = true;
    };
    Professional = {
      address = "adam.l.washington@gmail.com";
      flavor = "gmail.com";
      realName = "Adam Washington";
      passwordCommand = "${pkgs.pass}/bin/pass professional_email";
      msmtp.enable = true;
    };
    Work = {
      address = "adam.washington@stfc.ac.uk";
      imap.host = "outlook.office365.com";
      imap.port = 993;
      imap.tls.enable = true;
      imap.tls.useStartTls = true;
      offlineimap.extraConfig.remote.ssl = true;
      realName = "Adam Washington";
      passwordCommand = "${pkgs.pass}/bin/pass work_email | head -n 1";
      msmtp.enable = true;
      msmtp.extraConfig = {
        auth = "xoauth2";
        passwordeval =
          "${fixer}/refresh_token.py";
      };
      userName = "adam.washington@stfc.ac.uk";
      smtp.host = "smtp.office365.com";
      smtp.port = 587;
      smtp.tls.enable = true;
      smtp.tls.useStartTls = true;
    };
  };
  programs.msmtp.enable = true;
  home.packages = with pkgs; [ msmtp offlineimap davmail thunderbird ];
}
