{ pkgs, ... }:

{
  home.packages = [
    # Game stuff.  This should probably be its own file
    pkgs.dolphinEmu
    pkgs.dosbox
    pkgs.minecraft
    pkgs.openmw
    pkgs.renpy
    (pkgs.steam.override { nativeOnly = false; })
    (pkgs.retroarch.override {
      cores = [
        pkgs.libretro.beetle-psx
        pkgs.libretro.flycast
        pkgs.libretro.genesis-plus-gx
        pkgs.libretro.mgba
        pkgs.libretro.mupen64plus
        pkgs.libretro.opera
        pkgs.libretro.quicknes
        pkgs.libretro.snes9x
      ];
    })
  ];
}
