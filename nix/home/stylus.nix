{ colors }:

[
  {
    settings = {
      openEditInWindow = false;
      "openEditInWindow.popup" = false;
      windowPosition = { };
      show-badge = true;
      disableAll = false;
      exposeIframes = false;
      newStyleAsUsercss = true;
      styleViaXhr = false;
      patchCsp = false;
      "config.autosave" = true;
      "popup.breadcrumbs" = true;
      "popup.breadcrumbs.usePath" = false;
      "popup.enabledFirst" = true;
      "popup.stylesFirst" = true;
      "popup.autoResort" = false;
      "popup.borders" = false;
      "popup.findStylesInline" = true;
      "popup.findSort" = "u";
      "manage.onlyEnabled" = false;
      "manage.onlyLocal" = false;
      "manage.onlyUsercss" = false;
      "manage.onlyEnabled.invert" = false;
      "manage.onlyLocal.invert" = false;
      "manage.onlyUsercss.invert" = false;
      "manage.actions.expanded" = true;
      "manage.backup.expanded" = true;
      "manage.filters.expanded" = true;
      "manage.newUI" = true;
      "manage.newUI.favicons" = false;
      "manage.newUI.faviconsGray" = true;
      "manage.newUI.targets" = 3;
      "manage.newUI.sort" = "title,asc";
      "editor.options" = { };
      "editor.toc.expanded" = true;
      "editor.options.expanded" = true;
      "editor.lint.expanded" = true;
      "editor.publish.expanded" = true;
      "editor.lineWrapping" = true;
      "editor.smartIndent" = true;
      "editor.indentWithTabs" = false;
      "editor.tabSize" = 4;
      "editor.keyMap" = "default";
      "editor.theme" = "base16-light";
      "editor.beautify" = {
        "selector_separator_newline" = true;
        "newline_before_open_brace" = false;
        "newline_after_open_brace" = true;
        "newline_between_properties" = true;
        "newline_before_close_brace" = true;
        "newline_between_rules" = false;
        "preserve_newlines" = true;
        "end_with_newline" = false;
        "indent_conditional" = true;
      };
      "editor.beautify.hotkey" = "";
      "editor.lintDelay" = 300;
      "editor.linter" = "csslint";
      "editor.lintReportDelay" = 500;
      "editor.matchHighlight" = "token";
      "editor.autoCloseBrackets" = true;
      "editor.autocompleteOnTyping" = false;
      "editor.contextDelete" = false;
      "editor.selectByTokens" = true;
      "editor.appliesToLineWidget" = true;
      "editor.livePreview" = true;
      "editor.colorpicker" = true;
      "editor.colorpicker.hexUppercase" = false;
      "editor.colorpicker.hotkey" = "";
      "editor.colorpicker.color" = "";
      "editor.colorpicker.maxHeight" = 300;
      "hotkey._execute_browser_action" = "";
      "hotkey.openManage" = "";
      "hotkey.styleDisableAll" = "";
      "sync.enabled" = "none";
      iconset = 0;
      badgeDisabled = "#8B0000";
      badgeNormal = "#006666";
      popupWidth = 246;
      updateInterval = 24;
    };
  }
  {
    enabled = true;
    updateUrl = null;
    url = null;
    installDate = 1643629353458;
    sections = [{
      code = ''
        /*
         * Drop the below regex, after a comma, just before the opening curly bracket above, to exclude websites from solarization:
         * ,regexp("https?://(www\.)?(?!(userstyles\.org|docs\.google|github)\..*).*")
         */
            /* Firefox Scrollbars */
            scrollbar {
                opacity: .75 !important;
            }

            /*Vars
        color00   #${colors.base00}
        color01   #${colors.base01}
        color02   #${colors.base02}
        color03   #${colors.base03}
        color04   #${colors.base04}
        color05   #${colors.base05}
        color06   #${colors.base06}
        color07   #${colors.base07}
        color08   #${colors.base08}
        color15   #${colors.base0A}
        color16   #${colors.base0B}
        color17   #${colors.base0C}
        color18   #${colors.base0D}
        color19   #${colors.base0E}
        color20   #${colors.base0F}
        color21   #e5ebf1
        */
            /* Base */
            *,
            ::before,
            ::after {
                color: #${colors.base05} !important;
                border-color: #${colors.base0D} !important;
                outline-color: #${colors.base0D} !important;
                text-shadow: none !important;
                box-shadow: none !important;
                /*-moz-box-shadow: none !important;*/
                background-color: transparent !important;
            }

            html * {
                color: inherit !important;
            }

            p::first-letter,
            h1::first-letter,
            h2::first-letter,
            p::first-line {
                color: inherit !important;
                background: none !important;
            }

            /* :: Give solid BG :: */
            /* element */
            b,
            i,
            u,
            strong {
                color: #${colors.base06};
            }

            html,
            body,
            li ul,
            ul li,
            table,
            header,
            article,
            section,
            nav,
            menu,
            aside,

            /* common */
            [class*="nav"],
            [class*="open"],
            [id*="ropdown"],
            /*dropdown*/
            [class*="ropdown"],
            div[class*="menu"],
            [class*="tooltip"],
            div[class*="popup"],
            div[id*="popup"],

            /* Notes, details, etc.  Maybe useful */
            div[id*="detail"],
            div[class*="detail"],
            div[class*="note"],
            span[class*="note"],
            div[class*="description"],

            /* Also common */
            div[class*="content"],
            div[class*="container"],

            /* Popup divs that use visibility: hidden and display: none */
            div[style*="display: block"],
            div[style*="visibility: visible"] {
                background-color: #${colors.base00} !important;
            }



            /*: No BG :*/
            *:not(:empty):not(span):not([class="html5-volume-slider html5-draggable"]):not([class="html5-player-chrome html5-stop-propagation"]),
            *::before,
            *::after,
            td:empty,
            p:empty,
            div:empty:not([role]):not([style*="flashblock"]):not([class^="html5"]):not([class*="noscriptPlaceholder"]) {
                background-image: none !important;
            }

            /*: Filter non-icons :*/
            span:not(:empty):not([class*="icon"]):not([id*="icon"]):not([class*="star"]):not([id*="star"]):not([id*="rating"]):not([class*="rating"]):not([class*="prite"]) {
                background-image: none !important;
                text-indent: 0 !important;
            }

            /*: Image opacity :*/
            img,
            svg {
                opacity: .75 !important;
            }

            img:hover,
            svg:hover {
                opacity: 1 !important;
            }

            /* Highlight */
            ::-moz-selection {
                background-color: #e5ebf1 !important;
                color: #${colors.base0E} !important;
            }

            /* ::: anchor/links ::: */
            a {
                color: #${colors.base06} !important;
                background-color: #${colors.base00} !important;
                opacity: 1 !important;
                text-indent: 0 !important;
            }

            a:link {
                color: #${colors.base03} !important;
            }
            /* hyperlink */
            a:visited {
                color: #${colors.base05} !important;
            }

            a:hover {
                color: #${colors.base03} !important;
                background-color: #${colors.base0D} !important;
            }

            a:active {
                color: #${colors.base0B} !important;
            }

            /* "Top level" div */
            body > div {
                background-color: inherit !important;
            }

            /* :::::: Text Presentation :::::: */
            summary,
            details {
                background-color: inherit !important;
            }

            kbd,
            time,
            label,
            .date {
                color: #${colors.base02} !important;
            }

            acronym,
            abbr {
                border-bottom: 1px dotted !important;
                cursor: help !important;
            }

            mark {
                background-color: #${colors.base01} !important;
            }


            /* :::::: Headings :::::: */
            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                background-image: none !important;
                border-radius: 5px !important;
                /*-moz-border-radius: 5px !important;*/
                -webkit-border-radius: 5px !important;
                text-indent: 0 !important;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                background-color: #${colors.base0D} !important;
            }

            h1,
            h2 {
                color: #${colors.base02}!important;
            }

            h3,
            h4 {
                color: #${colors.base03}!important;
            }

            h5,
            h6 {
                color: #${colors.base0B}!important;
            }

            /* :::::: Tables, cells :::::: */
            table table {
                background: #${colors.base0D} !important;
            }

            th,
            caption {
                background: #${colors.base00} !important;
            }

            /* ::: Inputs, textareas ::: */
            input,
            textarea,
            button,
            select,
            option,
            optgroup {
                color: #${colors.base0E} !important;
                background: none #${colors.base0D} !important;
                -moz-appearance: none !important;
                -webkit-appearance: none !important;
            }

            input,
            textarea,
            button {
                border-color: #${colors.base0E} !important;
                border-width: 1px !important;
            }

            /* :::::: Button styling :::::: */
            input[type="button"],
            input[type="submit"],
            input[type="reset"],
            button {
                background: #${colors.base0D} !important;
            }

            input[type="button"]:hover,
            input[type="submit"]:hover,
            input[type="reset"]:hover,
            button:hover {
                color: #${colors.base0E} !important;
                background: #e5ebf1 !important;
            }

            input[type="image"] {
                opacity: .85 !important;
            }

            input[type="image"]:hover {
                opacity: .95 !important;
            }

            /* Lightbox fix */
            html [id*="lightbox"] * {
                background-color: transparent !important;
            }

            html [id*="lightbox"] img {
                opacity: 1 !important;
            }

            /* Youtube Annotation */
            #movie_player-html5 .annotation {
                background: #${colors.base0D} !important;
            }'';
      start = 296;
      urlPrefixes = [ "http://" "https://" ];
    }];
    customName = "base16";
    sourceCode = ''
      /* ==UserStyle==
      @name           Empty @name replaces the default template - 31/01/2022, 11:40:38
      @namespace      github.com/openstyles/stylus
      @version        1.0.0
      @description    A new userstyle
      @author         Me
      ==/UserStyle== */
      @-moz-document url-prefix("http://"),
      url-prefix("https://") {
          /*
       * Drop the below regex, after a comma, just before the opening curly bracket above, to exclude websites from solarization:
       * ,regexp("https?://(www\.)?(?!(userstyles\.org|docs\.google|github)\..*).*")
       */
          /* Firefox Scrollbars */
          scrollbar {
              opacity: .75 !important;
          }

          /*Vars
      color00   #${colors.base00}
      color01   #${colors.base01}
      color02   #${colors.base02}
      color03   #${colors.base03}
      color04   #${colors.base04}
      color05   #${colors.base05}
      color06   #${colors.base06}
      color07   #${colors.base07}
      color08   #${colors.base08}
      color15   #${colors.base0A}
      color16   #${colors.base0B}
      color17   #${colors.base0C}
      color18   #${colors.base0D}
      color19   #${colors.base0E}
      color20   #${colors.base0F}
      color21   #e5ebf1
      */
          /* Base */
          *,
          ::before,
          ::after {
              color: #${colors.base05} !important;
              border-color: #${colors.base0D} !important;
              outline-color: #${colors.base0D} !important;
              text-shadow: none !important;
              box-shadow: none !important;
              /*-moz-box-shadow: none !important;*/
              background-color: transparent !important;
          }

          html * {
              color: inherit !important;
          }

          p::first-letter,
          h1::first-letter,
          h2::first-letter,
          p::first-line {
              color: inherit !important;
              background: none !important;
          }

          /* :: Give solid BG :: */
          /* element */
          b,
          i,
          u,
          strong {
              color: #${colors.base06};
          }

          html,
          body,
          li ul,
          ul li,
          table,
          header,
          article,
          section,
          nav,
          menu,
          aside,

          /* common */
          [class*="nav"],
          [class*="open"],
          [id*="ropdown"],
          /*dropdown*/
          [class*="ropdown"],
          div[class*="menu"],
          [class*="tooltip"],
          div[class*="popup"],
          div[id*="popup"],

          /* Notes, details, etc.  Maybe useful */
          div[id*="detail"],
          div[class*="detail"],
          div[class*="note"],
          span[class*="note"],
          div[class*="description"],

          /* Also common */
          div[class*="content"],
          div[class*="container"],

          /* Popup divs that use visibility: hidden and display: none */
          div[style*="display: block"],
          div[style*="visibility: visible"] {
              background-color: #${colors.base00} !important;
          }



          /*: No BG :*/
          *:not(:empty):not(span):not([class="html5-volume-slider html5-draggable"]):not([class="html5-player-chrome html5-stop-propagation"]),
          *::before,
          *::after,
          td:empty,
          p:empty,
          div:empty:not([role]):not([style*="flashblock"]):not([class^="html5"]):not([class*="noscriptPlaceholder"]) {
              background-image: none !important;
          }

          /*: Filter non-icons :*/
          span:not(:empty):not([class*="icon"]):not([id*="icon"]):not([class*="star"]):not([id*="star"]):not([id*="rating"]):not([class*="rating"]):not([class*="prite"]) {
              background-image: none !important;
              text-indent: 0 !important;
          }

          /*: Image opacity :*/
          img,
          svg {
              opacity: .75 !important;
          }

          img:hover,
          svg:hover {
              opacity: 1 !important;
          }

          /* Highlight */
          ::-moz-selection {
              background-color: #e5ebf1 !important;
              color: #${colors.base0E} !important;
          }

          /* ::: anchor/links ::: */
          a {
              color: #${colors.base06} !important;
              background-color: #${colors.base00} !important;
              opacity: 1 !important;
              text-indent: 0 !important;
          }

          a:link {
              color: #${colors.base06} !important;
          }
          /* hyperlink */
          a:visited {
              color: #${colors.base04} !important;
          }

          a:hover {
              color: #${colors.base07} !important;
              background-color: #${colors.base01} !important;
          }

          a:active {
              color: #${colors.base0B} !important;
          }

          /* "Top level" div */
          body > div {
              background-color: inherit !important;
          }

          /* :::::: Text Presentation :::::: */
          summary,
          details {
              background-color: inherit !important;
          }

          kbd,
          time,
          label,
          .date {
              color: #${colors.base02} !important;
          }

          acronym,
          abbr {
              border-bottom: 1px dotted !important;
              cursor: help !important;
          }

          mark {
              background-color: #${colors.base01} !important;
          }


          /* :::::: Headings :::::: */
          h1,
          h2,
          h3,
          h4,
          h5,
          h6 {
              background-image: none !important;
              border-radius: 5px !important;
              /*-moz-border-radius: 5px !important;*/
              -webkit-border-radius: 5px !important;
              text-indent: 0 !important;
          }

          h1,
          h2,
          h3,
          h4,
          h5,
          h6 {
              background-color: #${colors.base01} !important;
          }

          h1,
          h2 {
              color: #${colors.base0D}!important;
          }

          h3,
          h4 {
              color: #${colors.base07}!important;
          }

          h5,
          h6 {
              color: #${colors.base06}!important;
          }

          /* :::::: Tables, cells :::::: */
          table table {
              background: #${colors.base01} !important;
          }

          th,
          caption {
              background: #${colors.base00} !important;
          }

          /* ::: Inputs, textareas ::: */
          input,
          textarea,
          button,
          select,
          option,
          optgroup {
              color: #${colors.base0E} !important;
              background: none #${colors.base01} !important;
              -moz-appearance: none !important;
              -webkit-appearance: none !important;
          }

          input,
          textarea,
          button {
              border-color: #${colors.base0E} !important;
              border-width: 1px !important;
          }

          /* :::::: Button styling :::::: */
          input[type="button"],
          input[type="submit"],
          input[type="reset"],
          button {
              background: #${colors.base01} !important;
          }

          input[type="button"]:hover,
          input[type="submit"]:hover,
          input[type="reset"]:hover,
          button:hover {
              color: #${colors.base0E} !important;
              background: #e5ebf1 !important;
          }

          input[type="image"] {
              opacity: .85 !important;
          }

          input[type="image"]:hover {
              opacity: .95 !important;
          }

          /* Lightbox fix */
          html [id*="lightbox"] * {
              background-color: transparent !important;
          }

          html [id*="lightbox"] img {
              opacity: 1 !important;
          }

          /* Youtube Annotation */
          #movie_player-html5 .annotation {
              background: #${colors.base01} !important;
          }
      }'';
    usercssData = {
      name = "Empty @name replaces the default template - 31/01/2022, 11:40:38";
      namespace = "github.com/openstyles/stylus";
      version = "1.0.0";
      description = "A new userstyle";
      author = "Me";
    };
    author = "Me";
    description = "A new userstyle";
    name = "Empty @name replaces the default template - 31/01/2022, 11:40:38";
    updateDate = 1643629353458;
    "_id" = "43d3f862-0b41-4ca3-b0d0-38ee1205c9b7";
    "_rev" = 1643629353458;
    id = 1;
  }
]
