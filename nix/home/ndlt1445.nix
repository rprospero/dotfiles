{ pkgs, lib, ... }:

{
  accounts.email.accounts.Work.primary = true;
  home.packages = [
    pkgs.pan
    pkgs.python
    pkgs.minecraft
    pkgs.texlive.combined.scheme-full
    pkgs.qbittorrent
  ];
  manual.json.enable = true;
  programs.noti.enable = true;
}
