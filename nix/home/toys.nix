{ pkgs, config, nasus, ... }:

{
  home.packages = with pkgs; [
    catimg
    cowsay
    fortune
    imagemagick
    imlib2
    jp2a
    libsixel
    neofetch
  ];
  programs.fish.functions = {
    hack =
      "${pkgs.bat}/bin/bat $argv --color always --style plain | ${pkgs.pv}/bin/pv -qL 150";
    pwn = "cat $argv | ${pkgs.lolcat}/bin/lolcat -f | ${pkgs.pv}/bin/pv -qL 1000";
    leet = "cat $argv | ${pkgs.lolcat}/bin/lolcat -a";
  };
  xdg.configFile.neofetch.source =
    config.lib.file.mkOutOfStoreSymlink
    "${config.home.homeDirectory}/Code/dotfiles/neofetch";
}
