let Filter : Type = { tags : Text, query : Text }

let IndexFilter : Type = { index : Natural, value : Filter }

let values = ./afew.dhall

let makeFilter =
	λ(x : IndexFilter)
      → ''
	[Filter.${Natural/show x.index}]
	tags = ${x.value.tags}
	query = ${x.value.query}
	''

let myfilters =
      List/fold
      IndexFilter
      (List/indexed Filter ./afew.dhall)
      Text
      (λ(x : IndexFilter) → λ(y : Text) → makeFilter x ++ y)
      ""

in  ''
    [SpamFilter]
    ${myfilters}
    [ArchiveSentMailsFilter]
    [InboxFilter]
    ''