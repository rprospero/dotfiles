{ pkgs, ... }:

let

  unstable = import (pkgs.fetchFromGitHub {
    owner = "nixos";
    repo = "nixpkgs";
    rev = "19.03-beta";
    sha256 = "0wdrfh6zrlsgsyhb0l08yfdfs43anf9x86f7lkz58nrwdnp5m0gf";
  }) { };
  fixed_font = [
    "xft:InconsolataGo Nerd Font Mono:pixelsize=14:antialias=true:hinting=true"
    "xft:dejavu sans mono:pixelsize=12:antialies=true:hinting=true"
  ];
  emacs_fixed_font = "InconsolataGo Nerd Font Mono-12";

in {
  imports = [ ./common.nix ./graphical.nix ];
  accounts.email.accounts.Work.primary = true;
  home.packages =
    [ pkgs.mutt unstable.sasview pkgs.texlive.combined.scheme-medium ];
  programs.emacs.package = unstable.emacs;
  xresources.properties."URxvt.font" = fixed_font;
  xresources.properties."Emacs.font" = emacs_fixed_font;
}
