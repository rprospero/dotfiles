{ pkgs, config, ... }:

let inherit (config) colors;
in {
  home.file = {
    ".mlterm/main".text = ''
      use_transbg = false
      wall_picture =
      bg_color = #${colors.base00}
      fg_color = #${colors.base05}
      type_engine = cairo
      scrollbar_mode = none
    '';
    ".mlterm/aafont".text = ''
      ISO10646_UCS4_1 = ${config.font-name} ${toString config.font-size}
      U+2013-DFFF = Symbola 16
      U+E000-FFFF = ${config.font-name} ${toString config.font-size}
    '';
    ".mlterm/color".text = ''
      red = #${colors.base08}
      hl_red = #${colors.base09}
      yellow = #${colors.base0A}
      hl_yellow = #${colors.base0A}
      green = #${colors.base0B}
      hl_green = #${colors.base0B}
      cyan = #${colors.base0C}
      hl_cyan = #${colors.base0D}
      blue = #${colors.base0E}
      hl_blue = #${colors.base0E}
      magenta = #${colors.base0F}
      hl_magenta = #${colors.base0F}
    '';
  };
  services.picom = {
    enable = true;
    fade = true;
    inactiveOpacity = "0.5";
  };

  xsession = {
    enable = true;
    numlock.enable = true;
    initExtra = ''
      _JAVA_AWT_WM_NONREPARENTING=1

      sh ~/setroot.sh
    '';
    windowManager.command = "${pkgs.openbox}/bin/openbox";
    windowManager.xmonad = {
      enable = false;
      #   enableContribAndExtras = true;
      #   extraPackages =
      #     (haskellPackages: [ haskellPackages.yaml haskellPackages.icon-fonts]);
    };
    windowManager.i3 = {
      enable = false;
      config.fonts = [ "Monoid Nerd Font Mono 12" ];
      config.modifier = "Mod4";
      config.terminal = "mlterm";
      config.bars = [ ];
      config.colors = { background = "#${colors.base00}"; };
      config.colors.focused = {
        background = "#${colors.base00}";
        border = "#${colors.base00}";
        childBorder = "#${colors.base00}";
        indicator = "#${colors.base08}";
        text = "#${colors.base03}";
      };
      config.keybindings = {
        # start a terminal
        "Mod4+Return" = "exec /usr/bin/env mlterm";

        # kill focused window
        "Mod4+Shift+q " = "kill";

        # start dmenu (a program launcher)
        "Mod4+d" = "exec /usr/bin/env rofi -show run";
        "Mod4+Shift+s" = "exec /usr/bin/env rofi -show ssh";
        "Mod4+g" = "exec /usr/bin/env rofi -show window";

        # change focus
        "Mod4+h" = "focus left";
        "Mod4+j" = "focus down";
        "Mod4+k" = "focus up";
        "Mod4+l" = "focus right";

        # alternatively, you can use the cursor keys:
        "Mod4+Left" = "focus left";
        "Mod4+Down" = "focus down";
        "Mod4+Up" = "focus up";
        "Mod4+Right" = "focus right";

        # move focused window
        "Mod4+Shift+h " = "move left";
        "Mod4+Shift+j " = "move down";
        "Mod4+Shift+k " = "move up";
        "Mod4+Shift+l " = "move right";

        # alternatively, you can use the cursor keys:
        "Mod4+Shift+Left " = "move left";
        "Mod4+Shift+Down " = "move down";
        "Mod4+Shift+Up " = "move up";
        "Mod4+Shift+Right " = "move right";

        # split in horizontal orientation
        "Mod4+semicolon" = "split h";

        # split in vertical orientation
        "Mod4+v" = "split v";

        # enter fullscreen mode for the focused container
        "Mod4+f" = "fullscreen toggle";

        # change container layout (stacked, tabbed, default)
        "Mod4+s" = "layout stacking";
        "Mod4+w" = "layout tabbed";
        "Mod4+e" = "layout default";

        # toggle tiling / floating
        "Mod4+Shift+space " = "floating toggle";

        # change focus between tiling / floating windows
        "Mod4+space" = "focus mode_toggle";

        # focus the parent container
        "Mod4+a" = "focus parent";

        # focus the child container
        #"Mod4+d" = "focus child";

        # switch to workspace
        "Mod4+1" = "workspace 1";
        "Mod4+2" = "workspace 2";
        "Mod4+3" = "workspace 3";
        "Mod4+4" = "workspace 4";
        "Mod4+5" = "workspace 5";
        "Mod4+6" = "workspace 6";
        "Mod4+7" = "workspace 7";
        "Mod4+8" = "workspace 8";
        "Mod4+9" = "workspace 9";
        "Mod4+0" = "workspace 10";
        # ..

        # move focused container to workspace
        "Mod4+Shift+1 " = "move workspace 1";
        "Mod4+Shift+2 " = "move workspace 2";
        "Mod4+Shift+3 " = "move workspace 3";
        "Mod4+Shift+4 " = "move workspace 4";
        "Mod4+Shift+5 " = "move workspace 5";
        "Mod4+Shift+6 " = "move workspace 6";
        "Mod4+Shift+7 " = "move workspace 7";
        "Mod4+Shift+8 " = "move workspace 8";
        "Mod4+Shift+9 " = "move workspace 9";
        "Mod4+Shift+0 " = "move workspace 10";
        # ...

        # reload the configuration file
        "Mod4+Shift+c " = "reload";
        # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
        "Mod4+Shift+r " = "restart";
        # exit i3 (logs you out of your X session)
        "Mod4+Shift+e " = "exit";

        "XF86HomePage " = "exec /usr/bin/env firefox";
        "XF86Tools " = "exec /usr/bin/env emacsclient -c";

      };
      # config.colors.unfocused.border = "#${colors.base00}";
      config.startup = [{ command = "/home/adam/Code/dotfiles/bar.sh"; }];
      config.window.titlebar = false;
    };
  };
}
