{ pkgs, ... }:

let

  local = import /home/adam/Code/nixpkgs { };
  unstable = import (pkgs.fetchFromGitHub {
    owner = "nixos";
    repo = "nixpkgs";
    rev = "19.03-beta";
    sha256 = "1wr6dzy99rfx8s399zjjjcffppsbarxl2960wgb0xjzr7v65pikz";
  }) { };

in {
  imports = [ ./common.nix ./local_mail.nix ./graphical.nix ];
  accounts.email.accounts.Work.primary = true;
  home.packages = [
    # local.sasview
    pkgs.texlive.combined.scheme-full
  ];
  systemd.user.services.travis = {
    Install.WantedBy = [ "graphical-session.target" ];
    Unit = { Description = "Travis build monitoring"; };
    Service = {
      ExecStart = "${pkgs.travis}/bin/travis monitor -n";
      Restart = "on-failure";
    };
  };
  systemd.user.services.reverse_tunnel = {
    Install.WantedBy = [ "default.target" ];
    Unit = { Description = "Reverse Tunnel"; };
    Service = {
      ExecStart = "${pkgs.openssh}/bin/ssh -NT -R 2222:localhost:22 home";
      Restart = "on-failure";
    };
  };
}
