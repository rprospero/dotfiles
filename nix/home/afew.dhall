let Filter : Type = { tags : Text, query : Text }

in  [ { tags =
	  "+science"
      , query =
	  "from:neutronsources.org"
      }
    , { tags =
	  "+promotional;+travel"
      , query =
	  "from:secretescapes.com or from:gogreentaxisltd.co.uk or from:oxfordkey.co.uk or from:oxfordbus.co.uk"
      }
    , { tags = "+promotional", query = "from:petplanet" }
    , { tags =
	  "+experiment"
      , query =
	  "from:isisuo@stfc.ac.uk subject:scheduled"
      }
    , { tags = "+sasview;", query = "folder:/SASView/" }
    , { tags = "+geniescans;", query = "from:travis-ci.org subject:PyGenie" }
    , { tags = "+geniescans;", query = "from:codacy subject:Scans" }
    , { tags = "+mantid;", query = "from:mantidproject.org" }
    , { tags = "+science", query = "folder:/Science/" }
    , { tags = "+promotional;", query = "folder:/Promotional/" }
    , { tags = "+rifftrax;", query = "folder:/Rifftrax/" }
    , { tags = "+food;", query = "folder:/Food/" }
    , { tags = "+games;", query = "folder:/Games/" }
    , { tags = "+RPG", query = "folder:/RPG/" }
    , { tags = "+social", query = "folder:/Community/" }
    , { tags = "+paperspam;", query = "from:ResearchGate" }
    , { tags = "+paperspam;", query = "from:Academia.edu" }
    , { tags = "+paperspam;", query = "from:scholarcitations" }
    , { tags = "+paperspam;", query = "from:mendeley" }
    , { tags = "+promotional;+charity;", query = "from:DonorsChoose.org" }
    , { tags =
	  "+promotional;"
      , query =
	  "from:PetPlanet.co.uk or from:\"Powerhouse Fitness\" or from:IMDB.com"
      }
    , { tags =
	  "+promotional;+games;"
      , query =
	  "(from:epicgames.com or from:humblebundle or from:failbettergames.com)"
      }
    , { tags =
	  "+promotional;"
      , query =
	  "(from:discover.com or from:cases.com or from:umrelief)"
      }
    , { tags = "+social", query = "(from:nextdoor.co.uk or from:linkedin.com)" }
    ]