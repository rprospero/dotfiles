#!/usr/bin/env fish

swaymsg -t get_tree | jq -r '.. | select(.name?!=null) | select(.type?=="con") | .name' | rofi -i -dmenu | read -l answer
swaymsg "[title=\"$answer\"] focus"
