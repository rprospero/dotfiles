{ config, ... }:

let inherit (config) colors;

in {
  xdg.configFile."qt5ct/colors/personal.conf".text = ''
    [ColorScheme]
    inactive_colors=#ff${colors.base0C}, #ff${colors.base01}, #ff${colors.base01}, #ff${colors.base05}, #ff${colors.base03}, #ff${colors.base04}, #ff${colors.base0E}, #ff${colors.base06}, #ff${colors.base05}, #ff${colors.base01}, #ff${colors.base00}, #ff${colors.base03}, #ff${colors.base02}, #ff${colors.base0E}, #ff${colors.base09}, #ff${colors.base08}, #ff${colors.base02}, #ff${colors.base05}, #ff${colors.base01}, #ff${colors.base0E}, #8f${colors.base0E}
    disabled_colors=#ff${colors.base0F}, #ff${colors.base01}, #ff${colors.base01}, #ff${colors.base05}, #ff${colors.base03}, #ff${colors.base04}, #ff${colors.base0F}, #ff${colors.base0F}, #ff${colors.base0F}, #ff${colors.base01}, #ff${colors.base00}, #ff${colors.base03}, #ff${colors.base02}, #ff${colors.base0E}, #ff${colors.base09}, #ff${colors.base08}, #ff${colors.base02}, #ff${colors.base05}, #ff${colors.base01}, #ff${colors.base0F}, #8f${colors.base0F}
    active_colors=#ff${colors.base0C}, #ff${colors.base00}, #ff${colors.base00}, #ff${colors.base04}, #ff${colors.base02}, #ff${colors.base03}, #ff${colors.base0E}, #ff${colors.base05}, #ff${colors.base04}, #ff${colors.base00}, #ff${colors.base00}, #ff${colors.base02}, #ff${colors.base01}, #ff${colors.base0E}, #ff${colors.base09}, #ff${colors.base08}, #ff${colors.base01}, #ff${colors.base04}, #ff${colors.base00}, #ff${colors.base0E}, #8f${colors.base0E}
  '';
}
