{ pkgs, lib, ... }: {
  imports = [ ./color-settings.nix ];
  colors = {
    base00 = "000000";
    base01 = "331e00";
    base02 = "663d00";
    base03 = "995b00";
    base04 = "cc7a00";
    base05 = "FF9900";
    base06 = "FFFFFF";
    base07 = "9966cc";
    base08 = "cc99cc";
    base09 = "9999cc";
    base0A = "cc6666";
    base0B = "ffcc99";
    base0C = "9999ff";
    base0D = "ff9966";
    base0E = "cc6699";
    base0F = "cc9966";
  };
  font-name = "Swiss911 UCm BT";
  font-size = 36;
}
