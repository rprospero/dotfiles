{ pkgs, lib, ... }: {
  imports = [ ./color-settings.nix ];
  colors = {
    base00 = "f0d8bc";
    base01 = "f3c5a2";
    base02 = "f7b08d";
    base03 = "fd637f";
    base04 = "b300c6";
    base05 = "7a16df";
    base06 = "0033f6";
    base07 = "0044ff";
    base08 = "dc003f";
    base09 = "ad4c00";
    base0A = "557300";
    base0B = "008126";
    base0C = "008497";
    base0D = "007fe9";
    base0E = "0061ef";
    base0F = "c100a5";
  };
}
