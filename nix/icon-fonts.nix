{ mkDerivation, base, stdenv, fetchFromGitLab }:
mkDerivation {
  pname = "icon-fonts";
  version = "0.2.2";
  src = fetchFromGitLab {
    owner = "rprospero";
    repo = "icon-fonts";
    rev = "v0.2.2";
    sha256 = "0lzxy3nr4kh0ffkw6r9j4xhbrpyi6ai0yqi0f1s5aai1w1hwyd62";
  };
  libraryHaskellDepends = [ base ];
  description = "Package for handling icon fonts in Haskell";
  license = stdenv.lib.licenses.bsd3;
}
