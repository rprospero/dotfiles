let

  # change this as necessary or wipe and use ENV vars
  credentials = {
    project = "personalserver-192413";
    serviceAccount = "642107470711-compute@developer.gserviceaccount.com";
    accessKey = toString /home/adam/Code/dotfiles/secrets/nixops/gce.pem;
  };

  gce = { resources, ...}:  {
    networking.firewall.allowedTCPPorts = [ 80 22 ];
    deployment.targetEnv = "gce";
    deployment.gce = credentials // {
      region = "us-east1-b";
      instanceType="f1-micro";
      tags = [ "public-http" ];
      network = resources.gceNetworks.lb-net;
      rootDiskSize = 10;
    };
  };

in {

  # create a network that allows SSH traffic(by default), pings
  # and HTTP traffic for machines tagged "public-http"
  resources.gceNetworks.lb-net = credentials // {
    addressRange = "192.168.4.0/24";
    firewall = {
      allow-http = {
        targetTags = [ "public-http" ];
        allowed.tcp = [ 80 443 ];
      };
      allow-ping.allowed.icmp = null;
    };
  };

  # by default, health check pings port 80, so we don't have to set anything
  resources.gceHTTPHealthChecks.plain-hc = credentials;

  # resources.gceTargetPools.backends = { resources, nodes, ...}: credentials // {
  #   region = "us-east1";
  #   healthCheck = resources.gceHTTPHealthChecks.plain-hc;
  #   machines = with nodes; [ backend1 ];
  # };

  # resources.gceForwardingRules.lb = { resources, ...}: credentials // {
  #   protocol = "TCP";
  #   region = "us-east1";
  #   portRange = "80";
  #   targetPool = resources.gceTargetPools.backends;
  #   description = "Alternative HTTP Load Balancer";
  # };

  backend1 = gce;

}
