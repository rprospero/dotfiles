with import <nixpkgs> { };

let

  backend = { config, pkgs, ... }: {
    services.tt-rss.enable = true;
    services.tt-rss.selfUrlPath = "https://tt-rss.rprosperoglobal.duckdns.org";
    services.tt-rss.pluginPackages = [ pkgs.tt-rss-plugin-ff-instagram ];
    services.tt-rss.registration.enable = true;
    services.tt-rss.virtualHost = "tt-rss.rprosperoglobal.duckdns.org";
    networking.firewall.allowedTCPPorts = [ 80 443 ];
    services.openssh.enable = true;
    services.ddclient = {
      enable = true;
      domains = [ "rprosperoglobal" ];
      password = "0a47cecd-b4fb-49bb-9b5f-10a92f8bc230";
      protocol = "duckdns";
      server = "www.duckdns.org";
    };
    services.nginx = {
      enable = true;
      virtualHosts = {
        "tt-rss.rprosperoglobal.duckdns.org" = {
          forceSSL = true;
          enableACME = true;
        };
      };
    };
    users.users.root.openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCuA4fIea/OjvChOXL065jot6dq83Gm+VzlxJZRu1Ljk8ZJwKsr0K5IlkZcy8bUEwLeYyfMY4TN6tawJ9xYgr5EyYrLsXA2mYBUw/xGx9CGpomyrifCtfsnkELGprsijz7vavNi8K/0afAJ0oP0U/Qwl8hwIC2CFbGSrnqepYp61libumhXvmmleyraC2ivB4lyHAWC+AcS1idmcb0mjCqNR9SPDRrivuDmO77CNfBIlQQ85yQshiY8/MzPsU1Gk8Q9HVF7b4E/culQCUFKLFr6R05Bfj1U30XfTIXUb4BS3O+kLU/zUrcYPntupGsTVFZ0k+TkgpoAQV2EM7fhdowJ adam@Walter"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXiOLhtL7acvHWALV9Z/lr1LJDyuYf4WiR5qg6qvNzmmvw8dosauoxn8iwesoN/qagEc3rejukNhAaQCD8UkNvf1KjQ5VemgPiGaYUfijtjdalpZ+c/l+T8U8wVoXZJJ9+imbP7H3U3mRsOIq2UaU8Xb1H+SbcGbWEg+z1T10TOAaNhfMXRqWB9s4/7PVmugNq1EfKegR/XMSYVxNxmN+NqoFfueBAszY+E4GM+4rPqJXVmZLh1WETAK/rDie+DbaFz4BfpeZUbF57Sq83iskBqlx1NySrTLnWb1Amk0y+7RM0TWd2K/tNMka4mhww9JmRJfpKY2kMeFYqUfv86PMV adam@nixos"
    ];
  };

in {
  network.description = "Load balancing network";

  backend1 = backend;
}
