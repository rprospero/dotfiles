# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # Use the GRUB 2 boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.initrd.kernelModules = [ "amdgpu" ];

  environment.systemPackages = [
    pkgs.exfat
  ];

  hardware.bluetooth.enable = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.driSupport32Bit = true;
  # hardware.opengl.extraPackages32 = with pkgs; [ driversi686Linux.amdvlk ];
  hardware.pulseaudio.support32Bit = true;
  hardware.opengl.extraPackages = with pkgs; [
    amdvlk
    rocm-opencl-icd
    rocm-opencl-runtime
  ];
  hardware.xpadneo.enable = true;

  networking.hostName = "Channing"; # Define your hostname.

  hardware.pulseaudio.package = pkgs.pulseaudioFull;

  networking.firewall.allowedTCPPorts = [
    139
    445
    548
    2222
    2389
    5050
    5910
    6600
    8080
    8384
    5800
    5900
    25565
    24642
    27036
    27037
  ];
  networking.firewall.allowedUDPPorts =
    [ 137 138 27031 27032 27033 27034 27035 27036 ];
  # services.airsonic = {
  #   enable = true;
  #   port = 5050;
  #   listenAddress = "192.168.1.217";
  # };

  # security.pam.services.sshd.googleAuthenticator.enable = true;

  services.ddclient = {
    enable = true;
    domains = [ "rprosperohome" ];
    protocol = "duckdns";
    server = "www.duckdns.org";
  };
  services.openssh.gatewayPorts = "yes";
  services.fail2ban = {
    enable = true;
    ignoreIP = [ "192.168.0.0/16" "130.246.57.10/8" ];
  };

  #systemd.mounts = [
  # {
  # what = "li@192.168.0.21:/JimNeighbors";
  # where = "/home/li/Isolde";
  # type = "fuse.sshfs";
  # options = "IdentityFile=/home/li/.ssh/id_rsa,allow_other,debug";
  # wantedBy = ["multi-user.target"];
  # }
  # {
  # what = "li@192.168.0.21:/external";
  # where = "/home/li/external";
  # type = "fuse.sshfs";
  # options = "IdentityFile=/home/li/.ssh/id_rsa,allow_other,debug";
  # wantedBy = ["multi-user.target"];
  # }
  # {
  # what = "li@192.168.0.21:/backup";
  # where = "/home/li/backup";
  # type = "fuse.sshfs";
  # options = "IdentityFile=/home/li/.ssh/id_rsa,allow_other,debug";
  # wantedBy = ["multi-user.target"];
  # }
  #];

  services.xserver.videoDrivers = [ "amdgpu" ];
  services.xserver.enable = true;

  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.sddm.autoNumlock = true;
  services.xserver.desktopManager.plasma5.enable = true;
  programs.sway.enable = true;

  # services.xserver.desktopManager.cinnamon.enable = true;
  # services.xserver.desktopManager.enlightenment.enable = true;
  # services.xserver.desktopManager.gnome3.enable = true;
  # services.xserver.desktopManager.pantheon.enable = true;
  # services.xserver.desktopManager.xfce.enable = true;

  fileSystems."/data" = {
    device = "/dev/disk/by-label/data";
    fsType = "xfs";
  };
  fileSystems."/seagate" = {
    device = "/dev/disk/by-label/Seagate";
    options = [ "uid=li" "gid=users" ];
    fsType = "exfat";
  };
  fileSystems."/backup" = {
    device = "/dev/disk/by-label/BackupDrive";
    options = [ "uid=li" "gid=users" ];
    fsType = "exfat";
  };

  services.samba.enable = true;
  services.samba.extraConfig = ''
    guest account = li
    map to guest = bad user
    load printers = no
    printcap name = /dev/null
    printing = bsd
  '';
  services.samba.shares = {
    seagate = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/seagate";
    };
    backup = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/backup";
    };
    downloads = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/home/li/Downloads";
    };
  };
  # services.netatalk.enable = true;
  # services.netatalk.extraConfig = ''
  #   guest account = li
  #   uam list = uams_guest.so
  # '';
  # services.netatalk.volumes = {
  #   seagate = {
  #     "hosts allow" = "192.168.0.0/16";
  #     path = "/seagate";
  #     "read only" = false;
  #     rwlist = "li";
  #   };
  # };
}
