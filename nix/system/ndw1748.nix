# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  sddm-sugar-dark = pkgs.stdenv.mkDerivation rec {
    pname = "sddm-sugar-dark-theme";
    version = "1.2";
    dontBuild = true;
    installPhase = ''
      mkdir -p $out/share/sddm/themes
      cp -aR $src $out/share/sddm/themes/sugar-dark
    '';
    src = pkgs.fetchFromGitHub {
      owner = "MarianArlt";
      repo = "sddm-sugar-dark";
      rev = "v${version}";
      sha256 = "0gx0am7vq1ywaw2rm1p015x90b75ccqxnb1sz3wy8yjl27v82yhb";
    };
  };

  sddm-slice = pkgs.stdenv.mkDerivation rec {
    pname = "sddm-slice-theme";
    version = "1.2";
    dontBuild = true;
    installPhase = ''
      mkdir -p $out/share/sddm/themes
      cp -aR $src/slice $out/share/sddm/themes/slice
    '';
    src = pkgs.fetchFromGitHub {
      owner = "rprospero";
      repo = "sddm-slice";
      rev = "629f10f";
      sha256 = "07i32c2z8ki9nvqj0jn11l4hygvq9j464i803051jm09gqk9da96";
    };
  };

in {
  imports = [ # Include the results of the hardware scan.
    /etc/nixos/hardware-configuration.nix
    ./configuration.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  users.extraUsers.adam.extraGroups =
    [ "audio" "wheel" "vboxuser" "vboxsf" "docker" ];
  users.extraUsers.adam.uid = 1000;

  fileSystems."/Documents" = {
    fsType = "vboxsf";
    device = "auv61894.CLRC";
    options = [ "rw" "nofail" ];
  };
  fileSystems."/ndxlarmor" = {
    fsType = "vboxsf";
    device = "NDXLARMOR";
    options = [ "rw" "nofail" ];
  };
  fileSystems."/archive" = {
    fsType = "cifs";
    options =
      [ "ro" "credentials=/archive.creds" "file_mode=0444" "dir_mode=0555" ];
    device = "//isisdatar80/inst$";
  };
  fileSystems."/babylon" = {
    fsType = "cifs";
    options =
      [ "rw" "credentials=/archive.creds" "file_mode=0444" "dir_mode=0555" ];
    device = "//olympic/Babylon5";
  };

  networking.hostName = "ndw1748";
  networking.firewall.allowedTCPPorts = [ 6600 8080 ];
  nix.trustedUsers = [ "root" "adam" ];
  virtualisation.virtualbox.guest.enable = true;
  virtualisation.docker.enable = true;

  services.ddclient = {
    enable = true;
    domains = [ "rprosperowork" ];
    protocol = "duckdns";
    server = "www.duckdns.org";
  };
  services.davfs2.enable = true;
  services.xserver.displayManager.sddm.theme = "slice";
  environment.systemPackages = [sddm-sugar-dark sddm-slice pkgs.qt5.full pkgs.exfat];
}
