{ pkgs, ... }:

let
  conf = (import ../../secrets/tokens.nix).pptp;

in {
  environment.etc."ppp/options".text = ''
    # Lock the port
    lock
    # Turn off compression protocols we know won't be used
    noauth
    nobsdcomp
    nodeflate
    # We won't do PAP, EAP, CHAP, or MSCHAP, but we will accept MSCHAP-V2
    # (you may need to remove these refusals if the server is not using MPPE)
    refuse-pap
    refuse-eap
    refuse-chap
    refuse-mschap
      '';
  environment.etc."ppp/chap-secrets".mode = "0600";
  environment.etc."ppp/chap-secrets".text = ''
    ${conf.domain}\\${conf.username} ${conf.name} ${conf.password}
  '';
  environment.etc."ppp/peers/${conf.name}".text = ''
    pty "pptp ${conf.server} --nolaunchpppd"
    name ${conf.domain}\\${conf.username}
    remotename ${conf.name}
    require-mppe-128
    file /etc/ppp/options
    ipparam ${conf.name}
      '';
  environment.systemPackages = [ pkgs.ppp pkgs.pptp ];
  networking.firewall.extraCommands = ''
    iptables -A INPUT -p 47 -j ACCEPT
  '';
}

