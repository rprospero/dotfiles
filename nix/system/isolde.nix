# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  # Use the GRUB 2 boot loader.
  boot.loader.systemd-boot.enable = true;

  nix.trustedUsers = [ "root" "adam" ];
  nixpkgs.config.allowUnfree = true;

  networking.hostName = "Isolde"; # Define your hostname.
  networking.networkmanager.enable = true;
  networking.firewall.allowedTCPPorts =
    [ 22 80 139 443 445 548 227 1119 3389 3632 8080 9091 ];
  networking.firewall.allowedUDPPorts = [ 137 138 548 227 ];

  fonts.enableDefaultFonts = true;

  # hardware.bluetooth.enable = true;
  hardware.opengl.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.opengl.extraPackages32 = with pkgs.pkgsi686Linux; [ libva ];
  hardware.pulseaudio.support32Bit = true;
  hardware.pulseaudio.package = pkgs.pulseaudioFull;

  environment.systemPackages = with pkgs; [ ntfs3g exfat qbittorrent ];

  # programs.slock.enable = true;

  security.sudo.wheelNeedsPassword = false;

  services.xserver.videoDrivers = [ "nvidia" "vesa" ];

  services.davfs2.enable = true;
  services.samba.enable = true;
  services.samba.extraConfig = ''
    guest account = li
    map to guest = bad user
    load printers = no
    printcap name = /dev/null
    printing = bsd
  '';
  services.samba.shares = {
    exteral = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/external";
    };
    downloads = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/home/li/Downloads";
    };
    Important = {
      browseable = "yes";
      "guest ok" = "yes";
      "read only" = false;
      path = "/Important";
    };
  };


  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?

}
