{ config, pkgs, ... }:
let fqdn = "gitea.${config.networking.domain}";
in {

  services.gitea = {
    enable = true;
    domain = fqdn;
    rootUrl = "https://${fqdn}/";
  };

  services.nginx = {
    enable = true;
    # only recommendedProxySettings are strictly required, but the rest make sense as well.
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    # Settings to setup what certificates are used for which endpoint.
    virtualHosts = {
      "${fqdn}" = {
        enableACME = true;
        forceSSL = true;
        locations."/".proxyPass = "http://[::1]:${toString config.services.gitea.httpPort}"; # without a trailing /
      };
    };
  };
}
