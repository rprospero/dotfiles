{ config, pkgs, ... }:

{
  nix.trustedUsers = [ "adam" ];

  users.extraUsers.adam = {
    extraGroups = [ "wheel" "networkmanager" "video" "docker" ];
    isNormalUser = true;
    createHome = true;
    hashedPassword =
      "$6$lEe7OklfTxBnX$ktLwIrGXaAZIZJZefFhpO6eyAoaR8OJjQI.NjuWx.p4SjYtyNhS83/1HczII70za3S3EHT6Ni5YM0yIeZWGrF0";
    shell = pkgs.fish;
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDdCijHfGoWVmBdXWYUE+HGO6SzvqELZiT+j3PqsA07izU5RGnKCoOWveYIz1P1W7itN/1BS49Ff9kbE3BwpZ+gmGzP1CODZg5Wg/gjwqTTcKbLWj5ZNzdefzvVzFFbF0OtXnaphh+BsDiJq51wagRAO5AlPXKYso7/PQMdNy6qETyQw9DAAzg3EH3ElMuk42KgA8oR15S0eueMmZwwqfGzTG7EetM0kRhxZaMYwTfHEm0SYSySoq3swtoQGNaINRbfLYFUBl9aCkU/ms1BzQEjbYMtgOC03I5C76eYXyYNAfxzPH3UHxZVZvaKeQ2ZOtHilbpgvgav6PxX75RvQZsZVSGxLojpHE0MkPcfblkzHP5GJnNjRx852BKb8RDlNOw6xHYYgeGw5R1Bm+M46F7wJjB8gK8OhdhAMo6YyNbIjrBv3R1Z/WB/OY+SKEQKUC7fuLInlTvLW/70U+BVeoqha5j2DoVf1maIgRsEoXf+FtuTfFwDDVjMu4u4ZDO4Zt0= adam@laptop"
    ];
  };

  programs.fish.enable = true;
}
