{ config, pkgs, ... }:

{

  users.extraUsers.li.extraGroups = [ "wheel" "networkmanager" "transmission" ];
  users.extraUsers.li.uid = 1001;
  users.extraUsers.li.isNormalUser = true;
}
