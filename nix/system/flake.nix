{
  description = "NixOS configuration";

  inputs = {
    # minecraft.url = "github:nixos/nixpkgs/nixos-19.03";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-20.09";
    home-manager.url = "github:nix-community/home-manager";
  };

  outputs = { home-manager, nixpkgs, ... }: {
    nixosConfigurations = {
      Isolde = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./hardware/isolde.nix
          ./configuration.nix
          ./isolde.nix
        ];
      };
      Channing = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./chan.nix
        ];
      };
    };
  };
}
