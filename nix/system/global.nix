{ config, myblog, book-site, ... }: {
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.ddclient = {
    enable = true;
    domains = [ "rprosperoglobal" ];
    passwordFile = "/var/ddtoken";
    protocol = "duckdns";
    server = "www.duckdns.org";
  };

  services.nginx = {
    enable = true;
    virtualHosts = {
      "blog.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;
        locations = { "/" = { root = myblog.defaultPackage.aarch64-linux; }; };
      };
      "collections.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;
        locations = { "/" = { root = book-site; }; };
      };
    };
  };

  security.acme.acceptTerms = true;
  security.acme.defaults.email = "rprospero@gmail.com";

  boot.cleanTmpDir = true;
  zramSwap.enable = true;
  networking.hostName = "instance-20220410-1948";
  networking.domain = "rprosperoglobal.duckdns.org";
  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDdCijHfGoWVmBdXWYUE+HGO6SzvqELZiT+j3PqsA07izU5RGnKCoOWveYIz1P1W7itN/1BS49Ff9kbE3BwpZ+gmGzP1CODZg5Wg/gjwqTTcKbLWj5ZNzdefzvVzFFbF0OtXnaphh+BsDiJq51wagRAO5AlPXKYso7/PQMdNy6qETyQw9DAAzg3EH3ElMuk42KgA8oR15S0eueMmZwwqfGzTG7EetM0kRhxZaMYwTfHEm0SYSySoq3swtoQGNaINRbfLYFUBl9aCkU/ms1BzQEjbYMtgOC03I5C76eYXyYNAfxzPH3UHxZVZvaKeQ2ZOtHilbpgvgav6PxX75RvQZsZVSGxLojpHE0MkPcfblkzHP5GJnNjRx852BKb8RDlNOw6xHYYgeGw5R1Bm+M46F7wJjB8gK8OhdhAMo6YyNbIjrBv3R1Z/WB/OY+SKEQKUC7fuLInlTvLW/70U+BVeoqha5j2DoVf1maIgRsEoXf+FtuTfFwDDVjMu4u4ZDO4Zt0= adam@laptop"
  ];
}
