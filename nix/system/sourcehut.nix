{ config, pkgs, ... }:
let fqdn = "sourcehut.${config.networking.domain}";
in {

  services.sourcehut = {
    enable = true;
    # git.enable = true;
    # man.enable = true;
    meta.enable = true;
    nginx.enable = true;
    # postfix.enable = true;
    postgresql.enable = true;
    redis.enable = true;
    settings = {
      "sr.ht" = {
        environment = "production";
        global-domain = fqdn;
        origin = "https://${fqdn}";
        # Produce keys with srht-keygen from sourcehut.coresrht.
        network-key = pkgs.writeText "network.key" ((import ../../secrets/tokens.nix).srht.network);
        service-key = pkgs.writeText "service.key" ((import ../../secrets/tokens.nix).srht.service);
      };
      webhooks.private-key = pkgs.writeText "webhooks.key" ((import ../../secrets/tokens.nix).srht.webhooks);
    };
  };


  security.acme.certs."${fqdn}".extraDomainNames = [
    "meta.${fqdn}"
    "man.${fqdn}"
    "git.${fqdn}"
  ];


  services.nginx = {
    enable = true;
    # only recommendedProxySettings are strictly required, but the rest make sense as well.
    recommendedTlsSettings = true;
    recommendedOptimisation = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    # Settings to setup what certificates are used for which endpoint.
    virtualHosts = {
      "${fqdn}".enableACME = true;
      "meta.${fqdn}".useACMEHost = fqdn;
      "man.${fqdn}".useACMEHost = fqdn;
      "git.${fqdn}".useACMEHost = fqdn;
    };
  };
}
