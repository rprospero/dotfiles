{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      ./configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;
  # Use the GRUB 2 boot loader.
  # boot.loader.grub.enable = true;
  # boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.

  # boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  nix.binaryCaches = ["http://192.168.0.33:8080" ];
  nix.trustedBinaryCaches = [ "http://192.168.0.33:8080" ];
  nix.requireSignedBinaryCaches = false;

  networking.hostName = "walter"; # Define your hostname.
  networking.networkmanager.enable = true;

  services.xserver.libinput.enable = true;
  services.xserver.xkbOptions = "ctrl:swapcaps";

  users.extraUsers.adam.extraGroups = ["wheel" "vboxuser" "vboxsf" "networkmanager"];
  users.extraUsers.adam.uid = 1000;

}
