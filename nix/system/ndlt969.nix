# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      ./configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.

  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  nix.binaryCaches = ["http://192.168.0.33:8080" ];
  nix.trustedBinaryCaches = [ "http://192.168.0.33:8080" ];
  nix.requireSignedBinaryCaches = false;

  networking.hostName = "ndlt969a"; # Define your hostname.

  users.extraUsers.adam.extraGroups = ["wheel" "vboxuser" "vboxsf"];
  users.extraUsers.adam.uid = 1000;

  fileSystems."/Documents" = {
    fsType = "vboxsf";
    device = "Documents";
    options = ["rw"];
  };

}
