{ config, pkgs, ... }:

{
  services.home-assistant = {
    enable = true;
    openFirewall = true;
    config = {
      homeassistant = {
        media_dirs.media = "/external/Videos/Movies";
        time_zone = "Europe/London";
      };
      default_config = {};
      met = {};
    };
  };
}
