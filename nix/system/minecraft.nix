{ config, pkgs, lib, ... }:

{
  nixpkgs.config.allowUnfree = true;

  services.minecraft-server = {
    enable = true;
    openFirewall = true;
    eula = true;
    # package = minecraft.minecraft-server_1_13_2;
  };

  services.borgbackup.jobs.minecraft = {
    paths = "/var/lib/minecraft";
    repo = "/Important/minecraft-backup";
    encryption = {
      mode = "repokey";
      passphrase = (import ../../secrets/tokens.nix).borgminecraftpassword;
    };
    prune.keep = {
      within = "1d";
      daily = 7;
      weekly = 4;
      monthly = -1;
    };
  };
}
