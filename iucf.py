import csv
import sys

def main(file_path):
    with open(file_path, "r", encoding="latin-1") as infile:
        for _ in range(4):
            infile.readline()
        reader = csv.reader(infile)
        info = list(reader)
    with open("latest.csv", "w") as outfile:
        outfile.write("date,payee,amount,\r\n")
        writer = csv.writer(outfile)
        for row in info:
            amount = 0
            if row[4]:
                amount -= float(row[4])
            if row[5]:
                amount -= float(row[5])
            writer.writerow([row[1], row[2], "$"+str(amount)])

if __name__ == "__main__":
    main(sys.argv[1])
