{
  inputs.nixpkgs.url = "nixpkgs/nixos-22.05";
  inputs.nextpkgs.url = "nixpkgs/nixos-22.05";
  inputs.unstable.url = "nixpkgs/nixos-unstable";
  inputs.home-manager.url = "github:rycee/home-manager/release-22.05";
  inputs.home-manager.inputs.nixpkgs.follows = "nixpkgs";
  inputs.emacs.url = "github:nix-community/emacs-overlay";
  inputs.emacs.inputs.nixpkgs.follows = "nixpkgs";
  inputs.pia.url = "git+https://git.sr.ht/~rprospero/nixos-pia?ref=development";
  inputs.pia.inputs.nixpkgs.follows = "nixpkgs";
  inputs.eww.url = "github:elkowar/eww";
  # inputs.eww.inputs.nixpkgs.follows = "nixpkgs";
  inputs.nur.url = "github:nix-community/NUR";
  inputs.nur.inputs.nixpkgs.follows = "nixpkgs";
  inputs.statix.url = "github:nerdypepper/statix";
  inputs.statix.inputs.nixpkgs.follows = "nixpkgs";
  inputs.myblog.url = "git+https://git.sr.ht/~rprospero/blog?ref=development";
  inputs.myblog.inputs.nixpkgs.follows = "nixpkgs";
  inputs.book-site.url = "path:/home/adam/Code/book-site/_site";
  inputs.book-site.flake = false;

  outputs = { self, home-manager, nixpkgs, unstable, emacs, pia, eww, nur
    , statix, myblog, book-site, nextpkgs}: {
      package = {
        usbstick =
          self.nixosConfigurations.usbstick.config.system.build.sdImage;
      };
      homeManagerConfigurations = {
        "adam@laptop" = home-manager.lib.homeManagerConfiguration {
          configuration = ./nix/home/ndlt1445.nix;
          system = "x86_64-linux";
          homeDirectory = "/home/adam";
          username = "adam";
          extraModules = [
            ./nix/home/common.nix
            ./nix/home/graphical.nix
            ./nix/home/wayland.nix
            ./nix/home/python.nix
            ./nix/home/music.nix
            ./nix/home/toys.nix
            ./nix/home/chat.nix
            ./nix/home/personal_science.nix
            ./nix/home/local_mail.nix
            ./secrets/home.nix
            ./secrets/gallery.nix
          ];
          extraSpecialArgs = { inherit nixpkgs emacs eww statix unstable nur; };
        };
        "adam@NDLT1445" = home-manager.lib.homeManagerConfiguration {
          configuration = ./nix/home/ndlt1445.nix;
          system = "x86_64-linux";
          homeDirectory = "/home/adam";
          username = "adam";
          extraModules = [
            ./nix/home/common.nix
            ./nix/home/fonts.nix
            ./nix/home/personal_science.nix
            ./secrets/home.nix
          ];
          extraSpecialArgs = { inherit emacs; };
        };
        "adam@isolde" = home-manager.lib.homeManagerConfiguration {
          configuration = ./nix/home/isolde.nix;
          system = "x86_64-linux";
          homeDirectory = "/home/adam";
          username = "adam";
          extraModules = [
            ./nix/home/common.nix
            ./nix/home/graphical.nix
            ./nix/home/x11.nix
            ./nix/home/python.nix
            ./nix/home/toys.nix
            ./nix/home/games.nix
            ./nix/home/personal_science.nix
            ./nix/home/music.nix
            ./secrets/home.nix
          ];
        };
        chan = home-manager.lib.homeManagerConfiguration {
          configuration = ./nix/home/chan.nix;
          system = "x86_64-linux";
          homeDirectory = "/home/adam";
          username = "adam";
          extraModules = [ ./nix/home/personal_science.nix ];
        };
      };

      nixosConfigurations.laptop = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./nix/hardware/ndlt1445.nix
          ./nix/system/ndlt1445.nix
          ./nix/system/adam.nix
          ./nix/system/graphical.nix
          ./nix/system/locale.nix
          ./nix/system/meta.nix
          ./nix/system/pptp.nix
          pia.nixosModule
          # ./nix/system/pia.nix
          ./secrets/system.nix
        ];
      };
      nixosConfigurations.global = nextpkgs.lib.nixosSystem {
        system = "aarch64-linux";
        modules = [
          ./nix/hardware/global.nix
          ./nix/system/global.nix
          ./nix/system/matrix.nix
          # ./nix/system/sourcehut.nix
          ./nix/system/gitea.nix
        ];
        specialArgs = { inherit myblog book-site; };
      };
      nixosConfigurations.compute =
        (nixpkgs.lib.makeOverridable nixpkgs.lib.nixosSystem) {
          system = "x86_64-linux";
          modules = [
            ./nix/system/compute.nix
            ./nix/system/adam.nix
            ./nix/system/locale.nix
            ./nix/system/meta.nix
          ];
        };
      nixosConfigurations.stick = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./nix/hardware/stick.nix
          ./nix/system/stick.nix
          ./nix/system/adam.nix
          ./nix/system/graphical.nix
          ./nix/system/locale.nix
          ./nix/system/meta.nix
        ];
      };
      nixosConfigurations.Isolde = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./nix/hardware/isolde.nix
          ./nix/system/isolde.nix
          ./nix/system/adam.nix
          ./nix/system/graphical.nix
          ./nix/system/li.nix
          ./nix/system/locale.nix
          ./nix/system/meta.nix
          ./nix/system/xbox.nix
          # ./nix/system/pia.nix
          ./nix/system/ssh.nix
          ./nix/system/home-assistant.nix
        ];
      };
      nixosConfigurations.Channing = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          ./nix/hardware/chan.nix
          ./nix/system/chan.nix
          ./nix/system/adam.nix
          ./nix/system/li.nix
          ./nix/system/locale.nix
          ./nix/system/minecraft.nix
          ./nix/system/meta.nix
          ./nix/system/ssh.nix
          ./secrets/system.nix
        ];
      };
    };
}
